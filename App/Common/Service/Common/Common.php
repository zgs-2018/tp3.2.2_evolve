<?php
namespace Common\Service\Common;

use Common\Provide\Singleton;
use Common\Service\Service;
use Common\Service\Traits\__CallStatic;
use Common\Service\Traits\Yar;

/**
 * Class Common
 * @package Common\Service\Common
 * @method static mobileArea($mobile)
 * @method static ipArea($ip)
 * @method static accountUrl()
 * @method static encrypt($origin)
 * @method static decrypt($crypted)
 * @method static UUID()
 * @method static random(int $length,int $type=1) 1:混合 2:纯数字 3:纯字母 4:小写字母 5: 大写字母
 */
class Common
{
    use Yar,__CallStatic;

    public function getCalledAccessor()
    {
        // yar rpc 服务
        return static::YarClient( C('RPC_MAP.common') );
    }
}