<?php
namespace Common\Service\Mail;

use Common\Service\Service;
use Common\Service\Traits\__CallStatic;
use Common\Service\Traits\Yar;

/**
 * Class Mail
 * @package Common\Service\Mail
 * @method static verify($to,array $config)
 * @method static leadNotify($to, array $config)
 * @method static button($to, array $params) subject,tip,message,buttonName,url,time
 */
class Mail
{
    use __CallStatic,Yar;

    public function getCalledAccessor()
    {
        // TODO: Implement registerHandle() method.
        return static::YarClient( C('RPC_MAP.mail') );
    }
}