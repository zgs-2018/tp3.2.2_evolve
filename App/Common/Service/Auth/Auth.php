<?php
namespace Common\Service\Auth;

use Common\Service\Traits\__CallStatic;

/**
 * Class Auth
 * @package Common\Service\Auth
 * @method static status()
 * @method static login(array $payload)
 * @method static localLogin($ticket,$openID)
 * @method static isMineToken(array $payload)
 * @method static isLoginRemote ($ticket=false)
 * @method static isLoginLocal()
 * @method static logout ()
 * @method static localLogout()
 * @method static remoteLogout()
 * @method static user()
 * @method static profile()
 * @method static openID()
 * @method static save(string $field, $value)
 * @method static getToken($key='token')
 * @method static buildToken($ticket,$SID,$openID)
 * @method static authorizeTicket($ticket,$openID)
 * @method static destroyTicket($ticket)
 * @method static refreshTicketExpire ($ticket=null,int $expire=null)
 * @method static getTicket()
 * @method static getTicketExpire ($ticket=null, $p=false)
 */
class Auth
{
    use __CallStatic;

    public function getCalledAccessor()
    {
        // TODO: Implement registerHandle() method.
        return Handle::getInstance();
    }

}