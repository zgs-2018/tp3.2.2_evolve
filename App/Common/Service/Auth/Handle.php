<?php
namespace Common\Service\Auth;

use Common\Model\UserModel;
use Common\Provide\Singleton;
use Common\Service\JWT\JWT;
use Common\Service\Redis\Redis;

class Handle extends Singleton
{
    // 远程存储 票 的标识（redis）
    private static $tickets            =   'tickets';

    /**
     * @ 授权状态
     * @return bool
     */
    public function status ()
    {
        return $this->isLoginLocal()
            && $this->isLoginRemote();
    }

    /**
     * @ 登录
     * @param $payload
     * @return bool
     */
    public function login ($payload)
    {
        // 当前标识是否合法 (是否是当前会话)
        // if( !$this->isMineToken( $payload ) ) return false;
        // 远程是否授权
        if( !$this->isLoginRemote( $payload['ticket'] ) )
            return false;
        // 本地登录
        $this->localLogin( $payload['ticket'], $payload['openID'] );
        return true;
    }

    /**
     * @ 本地登录
     * @param $ticket (服务端session id)
     * @param $openID (用户openid)
     * @return mixed
     */
    public function localLogin ($ticket, $openID)
    {
        // 本地登录
        session( $this->getLocalKey(), $openID );
        // 记录票信息
        return $this->setTicket( $ticket );
    }

    /**
     * @ 是否是我的token
     * @param $SID
     * @return bool
     */
    public function isMineToken ($payload)
    {
        $SID        =   $payload['SID'] ?? false;
        return ($SID === session_id());
    }

    /**
     * @ 票信息授权 (远程登录)
     * @param $suffix
     * @param $openID
     * @return mixed
     */
    public function authorizeTicket ($ticket,$openID)
    {
        // 授权票
        Redis::setnx( static::$tickets . '_' . $ticket, $openID );
        // 设置票过期时间
        return $this->refreshTicketExpire( $ticket );
    }

    /**
     * @ 摧毁票信息
     * @param $ticket
     * @return mixed
     */
    public function destroyTicket ($ticket)
    {
        if( !$ticket ) return false;
        return Redis::del( static::$tickets . '_' . $ticket );
    }

    /**
     * 刷新票的有效期 （需同步本地session有效期）
     * @param null $ticket
     * @param int|null $expire
     * @return bool
     */
    public function refreshTicketExpire ($ticket=null,int $expire=null)
    {
        $ticket             =   is_null($ticket)    ? $this->getTicket() : $ticket;
        $expire             =   3600;

        return $ticket && Redis::expire( static::$tickets . '_' . $ticket, $expire );
    }

    /**
     * @ 登出
     * @return bool
     */
    public function logout ()
    {
        // 销毁远程信息
        $this->remoteLogout();
        // 销毁本地信息
        $this->localLogout();
        return true;
    }

    /**
     * @ 远程登出
     * @return mixed
     */
    public function remoteLogout ()
    {
        $ticket         =   $this->getTicket();
        return $this->destroyTicket( $ticket );
    }

    /**
     * @ 本地登出
     * @return mixed
     */
    public function localLogout ()
    {
        // 销毁 本地 ticket
        session( 'ticket', null );
        // 销毁本地登录信息
        return session( $this->getLocalKey(), null );
    }

    /**
     * @ 当前用户信息
     * @return array|bool
     */
    public function user ()
    {
        if( !$this->isLoginLocal() ) return false;
        $model              =   new UserModel();
        return $model->current();
    }

    /**
     * profile信息
     * @return bool|mixed
     */
    public function profile ()
    {
        $user               =   $this->user();
        if( ! $user ) return false;
        $profile            =   $user['profile'];
        $profile['openid']  =   $user['openid'];
        return $profile;
    }

    /**
     * 修改当前用户信息
     * @param $field
     * @param $value
     * @return bool
     */
    public function save ($field, $value)
    {
        if( !$this->isLoginLocal() ) return false;
        $userModel          =   new UserModel();
        $result             =   $userModel->field($field)
            ->where(['openid'=>['eq', $this->openID()]])
            ->save([$field=>$value]);
        if( $result === false ) return false;
        return true;
    }

    /**
     * @ 用户 openID
     * @return bool|mixed
     */
    public function openID ()
    {
        return $this->isLoginLocal() ?  session( $this->getLocalKey() ) :   false;
    }

    /**
     * @ 获取token
     * @return bool
     */
    public function getToken ($key='token')
    {
        $token              =   I($key);
        if( $token && ($payload=JWT::parseToken( $token )) )
            return $payload;
        return false;
    }

    /**
     * @ 返回客户端 token
     * @param $ticket 票
     * @param $SID  客户端会话 id
     * @param $openID   用户openid
     * @return mixed
     */
    public function buildToken ($ticket, $SID, $openID)
    {
        $claims             =   [
            'ticket'            =>  $ticket,
            'SID'               =>  $SID,
            'openID'            =>  $openID,
        ];
        return JWT::createToken( $claims );
    }

    /**
     * @ 本地授权状态
     * @return mixed
     */
    public function isLoginLocal ()
    {
        return ( session( '?' . $this->getLocalKey() ) && session( '?ticket' ) );
    }

    /**
     * @ 远程授权状态
     * @return bool
     */
    public function isLoginRemote ($ticket=false)
    {
        $ticket         =   $ticket ?:   $this->getTicket();
        return $ticket
            ?   Redis::get( static::$tickets . '_' . $ticket )
            :   false;
    }

    /**
     * @ 本地key标识
     * @return mixed
     */
    public function getLocalKey ()
    {
        return C( 'APP_ID' );
    }

    /**
     * @ 获取本地存储的 票信息
     * @return mixed
     */
    public function getTicket ()
    {
        return session( 'ticket' );
    }

    /**
     * @ 记录 票信息到本地
     * @param $ticket
     * @return mixed
     */
    public function setTicket ($ticket)
    {
        return session( 'ticket', $ticket );
    }

    /**
     * 获取 票有效期
     * @param null $ticket
     * @param bool $p
     * @return bool
     */
    public function getTicketExpire ($ticket=null, $p=false)
    {
        $ticketKey             =   $ticket ?: ( static::$tickets . '_' . $this->getTicket() );
        return $ticketKey
            ?   ( !$p ? Redis::ttl( $ticketKey ) : Redis::pttl( $ticketKey ) )
            :   false;
    }
}