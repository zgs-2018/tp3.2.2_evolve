<?php
namespace Common\Service\JWT;

use Common\Service\Service;
use Common\Service\Traits\__CallStatic;
use Common\Service\Traits\Yar;

/**
 * Class JWT
 * @package Common\Service\JWT
 * @method static createToken(array $customClaims=[])
 * @method static parseToken (string $token)
 */
class JWT
{
    use __CallStatic,Yar;

    public function getCalledAccessor()
    {
        // TODO: Implement registerHandle() method.
        return static::YarClient( C('RPC_MAP.jwt') );
    }
}