<?php
namespace Common\Service\JWT;

use Common\Provide\Singleton;

class FirebaseHandle extends Singleton
{
    /**
     * @var \Firebase\JWT;
     */
    private $instance   =   null;

    /**
     * @return \Firebase\JWT
     */
    public function getJWTInstance ()
    {
        if( $this->instance )
            return $this->instance;
        vendor( 'Firebase/JWT' );
        $this->setJWTInstance( new \Firebase\JWT() );
        return $this->instance;
    }

    /**
     * 储存实例
     * @param $instance
     * @return mixed
     */
    public function setJWTInstance ($instance)
    {
        return $this->instance      =   $instance;
    }
}