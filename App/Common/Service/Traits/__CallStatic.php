<?php
namespace Common\Service\Traits;

use Think\Log;
trait __CallStatic
{
    public static function __callStatic($name, $arguments)
    {
        try{
            //
            $handle         =   (new static())->getCalledAccessor();
            $result         =   $handle->{$name}(...$arguments);
            return $result;
        }catch (\Exception $exception){
            $message                =   '';
            if( $exception instanceof \Yar_Server_Exception){
                $message            .=  "\n" . 'message:' . $exception->getMessage() . "\r\n";
                $message            .=  'file:' . $exception->getFile() . "\r\n";
                $message            .=  'code:' . $exception->getCode() . "\r\n";
                $message            .=  'line:' . $exception->getLine() . "\r\n";
                Log::write( $message, Log::ERR, 'File', LOG_PATH . 'RPC.log' );
            }
            if( APP_DEBUG ){
                Ajax( $message, 'EVAL' );
            }else{
                return false;
            }
        }
    }

    abstract public function getCalledAccessor ();
}