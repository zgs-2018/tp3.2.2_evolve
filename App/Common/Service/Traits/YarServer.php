<?php
namespace Common\Service\Traits;

trait YarServer
{
    public function registerYarServer ( $class )
    {
        $yarService             =   new \Yar_server( $class );
        $yarService->handle();
    }
}