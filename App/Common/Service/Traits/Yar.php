<?php
namespace Common\Service\Traits;

trait Yar
{
    /**
     * @ yar 客户端实例
     * @param $rpc_uri
     * @param $domain string|boolean
     * @return \Yar_Client
     */
    public static function YarClient ($rpc_uri,$domain=false)
    {
        $domain             =   $domain ?: C('RPC_ADDRESS');
        $remote_address     =   $domain . $rpc_uri;
        return new \Yar_Client( $remote_address );
    }
}