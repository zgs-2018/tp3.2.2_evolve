<?php
namespace Common\Service\Str;

use Common\Service\Traits\__CallStatic;

/**
 * Class Str
 * @package Common\Service\Str
 * @method static buildPassword ($password, $algo=PASSWORD_BCRYPT)
 * @method static checkPassword ($password, $hash)
 * @method static random ($length, $type=1)
 * @method static ssid($user_id, $leeway='C')
 */
class Str
{
    use __CallStatic;

    public function getCalledAccessor()
    {
        // TODO: Implement getCalledAccessor() method.
        return Handle::getInstance();
    }
}