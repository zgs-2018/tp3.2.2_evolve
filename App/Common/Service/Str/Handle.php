<?php
namespace Common\Service\Str;

use Common\Provide\Singleton;
use Common\Service\Auth\Auth;
use Org\Util\Date;

class Handle extends Singleton
{
    /**
     * 创建密码
     * @param $password
     * @param int $algo
     * @return bool|string
     */
    public function buildPassword ($password, $algo=PASSWORD_BCRYPT)
    {
        return password_hash( $password, $algo );
    }

    /**
     * 密码验证
     * @param $password
     * @param $hash
     * @return bool
     */
    public function checkPassword ($password, $hash)
    {
        return password_verify( $password, $hash );
    }

    /**
     * @ 获取随机数
     * @param $length
     * @param int $type 1:混合 2:纯数字 3:纯字母 4:小写字母 5: 大写字母
     * @return string
     */
    public function random ($length, $type=1)
    {
        $maps           =   [
            range( 0, 9 ), range( 'a', 'z' ), range( 'A', 'Z' )
        ];
        // 获取随机标本
        switch ($type){
            case 1:
                $random     =   array_merge( $maps[0], $maps[1], $maps[2] );
                break;
            case 2:
                $random     =   $maps[0];
                break;
            case 3:
                $random     =   array_merge( $maps[1], $maps[2] );
                break;
            case 4:
                $random     =   $maps[1];
                break;
            case 5:
                $random     =   $maps[2];
                break;
        }
        // 打乱
        shuffle($random);
        $string             =   '';
        // 循环取值
        for( $i=1;$i<=$length;$i++ ){
            $string .= $random[ array_rand($random, 1) ];
        }
        return $string;
    }

    /**
     * @param $user
     * @param string $leeway C: 常规 S: 特殊 V:vip
     * @return string
     */
    public function ssid ($user_id,$leeway='C')
    {
        //  学号格式    { year+600 ~ date ~ `fill 0` ~ user_id ~ leeway }
        if( ! $user_id )    return false;
        //  当前year
        $current_year           =   date('Y');
        //  当前日期
        $current_date           =   date('md');
        //  尾部号码
        $tail_code              =   str_repeat('0', 6-strlen( $user_id )) . $user_id;
        //
        $data                   =   [
            'year'      =>  $current_year + 600,
            'date'      =>  $current_date,
            'tail'      =>  $tail_code,
            'leeway'    =>  strtoupper( $leeway )
        ];

        return implode( '', $data );
    }
}