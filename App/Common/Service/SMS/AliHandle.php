<?php
namespace Common\Service\SMS;

use Common\Provide\Singleton;
use Common\Service\Traits\Yar;
use Common\Service\Validate\Check;
use Think\Log;

class AliHandle extends Singleton
{
    use Yar;

    /**
     * @var \Yar_Client()
     */
    protected $alisms       =   null;

    /**
     * 发送验证码
     * @param mixed ...$args
     * @return bool
     */
    public function verify (...$args)
    {
        //  兼容调试模式
        if( APP_DEBUG ) return true;
        list($mobile, $code)    =   $args;
        //  选择短信模板
        $templates              =   C('sms.templates');
        $template_code          =   Check::mobile($mobile) || (substr($mobile, 0, 4)=='0086')
            ?   $templates['china']
            :   $templates['abroad'];
        //  发送
        $result     =   $this->getAlismsInstance()->verify($mobile, $code, $template_code);
        if( $result->Code == 'OK' ) {
            return true;
        }
        //  记录日志
        $message    =   $args[0] . '::' . $result->Message . '~' . $result->Code . '~' . $result->RequestId;
        Log::write( $message, Log::ERR, 'File', LOG_PATH . 'SMS.log' );
        return false;
    }

    /**
     * 获取 alisms 实例
     * @return AliHandle|\Yar_Client
     */
    public function getAlismsInstance ()
    {
        if( ! $this->alisms ) {
            $this->alisms       =   static::YarClient( C('RPC_MAP.sms') );
        }
        return $this->alisms;
    }
}