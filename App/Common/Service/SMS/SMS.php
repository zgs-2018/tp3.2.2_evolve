<?php
namespace Common\Service\SMS;

use Common\Service\Traits\__CallStatic;

/**
 * Class SMS
 * @package Common\Service\SMS
 * @method static verify($mobile,$code,$tpl_code='SMS_144450305')
 */
class SMS
{
    use __CallStatic;

    public function getCalledAccessor()
    {
        // TODO: Implement registerHandle() method.
        return AliHandle::getInstance();
    }
}