<?php
namespace Common\Service\Verify;

use Common\Provide\Singleton;
use Common\Service\Mail\Mail;
use Common\Service\SMS\SMS;

class Handle extends Singleton
{
    public static $exp      =   1200;

    protected $errors        =   [];

    /**
     * @ 手机短信验证码发送
     * @param $action
     * @param $mobile
     * @param $code
     * @param null $expire
     * @return bool|mixed
     */
    public function mobile ( $action ,$mobile, $code, $tpl_code='SMS_139415310', $expire=null)
    {
        // 设置有效期
        !is_null($expire) && $this->setExp( $expire );
        // 发送验证码
        $sended         =   SMS::verify( $mobile, $code, $tpl_code );
        if( $sended === false ) return false;
        // 存储记录
        return $this->storage( $action, 'mobile', $mobile, $code );
    }

    /**
     * @ 邮件验证码发送
     * @param $action
     * @param $email
     * @param $code
     * @param null $expire
     * @return bool|mixed
     */
    public function email ( $action, $email, $code, $expire=null )
    {
        // 设置有效期
        !is_null($expire) && $this->setExp( $expire );
        // 发送验证码
        $sended         =   Mail::verify( $email, ['code'=>$code] );
        if( $sended === false ) return false;
        // 存储记录
        return $this->storage( $action, 'email', $email, $code );
    }

    /**
     * @ 验证码验证
     * @param $sign : $action . '-' . $type
     * @param $value
     * @return bool
     */
    public function check ($sign,$mobile,$value,$destroy=false)
    {
        // 是否存在合法的验证码
        if( !($store = $this->legal( $sign, $mobile )) )
            return false;
        // 错误
        if( $store['value']!==$value ){
            $this->error( '验证码不正确', -1 );
            return false;
        }
        // 正确
        //      摧毁
        $destroy && $this->destroy($sign);
        return true;
    }

    /**
     * @ 是否存在合法的验证码
     * @param $sign
     * @param $mobile
     * @return bool|int|mixed|null
     */
    public function legal ($sign, $mobile)
    {
        // 获取存储数据
        $store              =   $this->get( $sign );
        // 不存在
        if( !$store )   return $store;
        // 手机号不匹配
        if( $store['key']!==$mobile ){
            $this->error( '手机号码不匹配', -9 );
            return false;
        }
        // 过期
        if( $store['exp'] < time() ){
            $this->error( '验证码已过期', -7 );
            return false;
        }
        // 合法
        return $store;
    }

    /**
     * @ 获取存储的验证信息
     * @param $action
     * @param null $type
     * @return mixed|null
     */
    public function get ($action, $type=null)
    {
        if( session( '?' . $this->getFinalKey($action, $type) ) ){
            return session( $this->getFinalKey($action, $type) );
        }else{
            $this->error( '验证码不存在', 0 );
            return null;
        }
    }

    /**
     * @ 摧毁存储的验证信息
     * @param $action
     * @param null $type
     * @return bool|mixed
     */
    public function destroy ($action, $type=null)
    {
        if( is_null($action) )
            return session( 'verify', null );
        return session( '?' . $this->getFinalKey( $action, $type ) )
            ?   session( $this->getFinalKey( $action, $type ), null )
            :   false;
    }

    /**
     * @param null $error
     * @param int $status -9:key(电话、邮件)不匹配 -7:已过期 -4:超出限制 -1:不正确 0:不存在
     * @return null
     */
    public function error ($error=null, $status=0)
    {
        if( is_null($error) ){
            return $this->errors;
        }else{
            $this->errors           =   compact( 'error', 'status' );
            return $this->errors;
        }
    }

    /**
     * @ 存储记录
     * @param $action
     * @param $type
     * @param $key
     * @param $value
     * @return mixed
     */
    public function storage ($action, $type, $key,  $value)
    {
        $finalKey               =   $this->getFinalKey($action, $type);
        $store                  =   [];
        $now                    =   time();
        if( session( '?' . $finalKey ) ){
            // 已发送过
            $store                =   session( $finalKey );
            if( $store['i'] > 7 ){
                $this->error('发送次数超出限制',-4);
                return false;
            }
            $store['key']         =   $key;
            $store['value']       =   $value;
            $store['i']           =   ++$store['i'];
            $store['exp']         =   $now + $this->getExp();
            $store['at']          =   $now;
        }else{
            $store              =   [
                'key'               =>  $key,
                'value'             =>  $value,
                'i'                 =>  1,
                'exp'               =>  $now + $this->getExp(),
                'at'                =>  $now
            ];
        }
        return session( $finalKey, $store );
    }

    /**
     * @ 获取操作key
     * @param $action
     * @param $type
     * @return string
     */
    public function getFinalKey ($action, $type=null)
    {
        return is_null($type)
            ?   'verify.' . $action
            :   'verify.' . $action . '-' . $type;
    }

    /**
     * @ 获取同意有效期
     * @return int
     */
    public function getExp ()
    {
        return static::$exp;
    }

    /**
     * @ 设置统一有效期
     * @param $expire
     * @return mixed
     */
    public function setExp ($expire)
    {
        return ( static::$exp=$expire );
    }
}