<?php
namespace Common\Service\Verify;

use Common\Service\Traits\__CallStatic;

/**
 * Class Verify
 * @package Common\Service\Verify
 * @method static mobile ( $action ,$mobile, $code, $tpl_code='SMS_139415310', $expire=null)
 * @method static email ( $action, $email, $code, $expire=null )
 * @method static check ($sign,$mobile,$value,$destroy=false)
 * @method static legal ($sign, $mobile)
 * @method static get ($action, $type=null)
 * @method static destroy ($action, $type=null)
 * @method static error ($error=null, $status=0)
 */
class Verify
{
    use __CallStatic;

    public function getCalledAccessor()
    {
        // TODO: Implement registerHandle() method.
        return Handle::getInstance();
    }
}