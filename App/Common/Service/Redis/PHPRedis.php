<?php
namespace Common\Service\Redis;

use Common\Provide\Singleton;

class PHPRedis extends Singleton
{
    /**
     * predis 原始实例
     * @var \Redis
     */
    protected $predisInstance        =   null;

    public function __construct()
    {
        //   初始化
        $this->init();
    }

    /**
     * 初始化 构造predis 实例
     * @param null $options
     */
    public function init ($options=null)
    {
        //  实例参数
        $options        =   $options ?: $this->getDefaultOptions();
        //  连接
        $redis          =   new \Redis();
        $redis->pconnect($options['host'], $options['port'], $options['timeout']);
        //  授权
        $options['pass'] && $redis->auth($options['pass']);
        //  切换数据库
        $options['db'] && $redis->select( $options['db'] );
        //  存储实例
        $this->predisInstance       =   $redis;
    }

    /**
     * 获取PHP redis 实例
     * @return \Redis
     */
    public function getPHPRedisInstance ()
    {
        $instance           =   $this->predisInstance;
        //  验证实例
        if( $instance && $instance->ping()==='+PONG' )
            return $instance;
        E('Redis is failed.');
    }

    /**
     * 默认配置参数
     * @return array
     */
    protected function getDefaultOptions ()
    {
        return  [
            'host'          =>  C('REDIS_HOST'),
            'port'          =>  C('REDIS_PORT'),
            'pass'          =>  C('REDIS_PASS'),
            'timeout'       =>  C('REDIS_TIMEOUT'),
            'db'            =>  C('REDIS_DB'),
        ];
    }
}