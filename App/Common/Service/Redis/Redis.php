<?php
namespace Common\Service\Redis;

use Common\Service\Traits\__CallStatic;
use Common\Service\Traits\Yar;

/**
 * Class Redis
 * @package Common\Service\Redis
 * @method static get( $key )
 * @method static set( $key, $value, $timeout = 0 )
 * @method static del( $key1, $key2 = null, $key3 = null )
 * @method static exists( $key )
 * @method static incr( $key )
 * @method static incrBy( $key, $value )
 * @method static decr( $key )
 * @method static decrBy( $key, $value )
 * @method static ttl( $key )
 * @method static pttl( $key )
 * @method static persist( $key )
 * @method static setex( $key, $ttl, $value )
 * @method static setnx( $key, $value )
 * @method static strlen( $key )
 * @method static expire( $key, $ttl )
 * @method static ping()
 */
class Redis
{
    use __CallStatic,Yar;

    /**
     * @time 2018.09.05
     * php redis 处理
     * @return \Redis
     */
    public function getCalledAccessor()
    {
        // TODO: Implement registerHandle() method.
        return PHPRedis::getInstance()->getPHPRedisInstance();
    }

    /**
     * yar 处理
     * @return \Yar_Client
     */
    public function getCalledAccessor_yar()
    {
        // TODO: Implement registerHandle() method.
        return static::YarClient( C('RPC_MAP.REDIS') );
    }
}