<?php
namespace Common\Service\Header;

use Common\Provide\Singleton;

class Handle extends Singleton
{
    /**
     * @ HTTP授权的授权证书
     * @param $authstring
     * @param bool $replace
     */
    public function authorization ( $authstring, $replace=false )
    {
        return $this->set( 'Authorization', 'Basic ' . $authstring, $replace );
    }

    /**
     * @param $url
     */
    public function location ($url)
    {
        $this->set( 'Location', ' ' . $url );exit;
    }

    /**
     * @param $string
     * @param null $value
     * @param bool $replace
     * @param int $http_response_code
     */
    public function set ( $string, $value=null, $replace=false, $http_response_code=200 )
    {
        return is_null( $value )
            ?   header( $string, $replace ,$http_response_code )
            :   header( $string . ':' .$value , $replace, $http_response_code  );
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function get ($name)
    {
        return exists_key( strtoupper($name), $alls=$this->all() )
            ?   $alls[strtoupper($name)]
            :   null;
    }

    /**
     * @return array
     */
    public function all ()
    {
        $headers            =   [];
        foreach ($_SERVER as $key => $value) {
            if ('HTTP_' == substr($key, 0, 5)) {
                $headers[str_replace('_', '-', substr($key, 5))] = $value;
            }
        }
        if (isset($_SERVER['PHP_AUTH_DIGEST'])) {
            $headers['AUTHORIZATION'] = $_SERVER['PHP_AUTH_DIGEST'];
        } elseif (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
            $headers['AUTHORIZATION'] = base64_encode($_SERVER['PHP_AUTH_USER'] . ':' . $_SERVER['PHP_AUTH_PW']);
        }
        if (isset($_SERVER['CONTENT_LENGTH'])) {
            $headers['CONTENT-LENGTH'] = $_SERVER['CONTENT_LENGTH'];
        }
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $headers['CONTENT-TYPE'] = $_SERVER['CONTENT_TYPE'];
        }
        return $headers;
    }
}