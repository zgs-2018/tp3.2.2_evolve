<?php
namespace Common\Service\Header;

use Common\Service\Traits\__CallStatic;

/**
 * Class Header
 * @package Common\Service\Header
 * @method static set ( $string, $value=null, $replace=false, $http_response_code=200 )
 * @method static authorization ( $authstring, $replace=false )
 * @method static location ($url)
 * @method static all()
 * @method static get($name)
 */
class Header
{
    use __CallStatic;

    public function getCalledAccessor()
    {
        return Handle::getInstance();
    }
}