<?php
namespace Common\Service\Curl;

use Common\Provide\Singleton;

class Handle extends Singleton
{
    public $curl;

    public $error;

    /**
     * @param $url
     * @param array $data
     * @param bool $return
     * @return array|mixed
     */
    public function get ($url, $data=[], $return=true)
    {
        $url                    =   $data
            ?   $url . '?' . (is_array($data) ? http_build_query($data) : $data)
            :   $url;
        //  初始化
        $this->curl = $curl = curl_init();
        //  设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url);
        //  设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, false);
        //  设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, $return);
        //  执行命令
        $exec = curl_exec($curl);
        if( $exec === false ){
            // 错误代号
            $errno              =   curl_errno($curl);
            // 错误信息
            $error              =   curl_error($curl);
            $this->setError( compact('errno', 'error') );
        }
        //  关闭URL请求
        curl_close($curl);
        //  返回
        return $exec;
    }

    /**
     * @param $url
     * @param array $data
     * @param bool $return
     * @return array|mixed
     */
    public function post ($url, $data=[], $return=true)
    {
        //  初始化
        $this->curl = $curl = curl_init();
        //  设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url);
        //  设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, false);
        //  设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, $return);
        //  设置post方式提交
        curl_setopt($curl, CURLOPT_POST, 1);
        //  设置post数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //  执行命令
        $exec = curl_exec($curl);
        if( $exec === false ){
            // 错误代号
            $errno              =   curl_errno($curl);
            // 错误信息
            $error              =   curl_error($curl);
            $this->setError( compact('errno', 'error') );
        }
        //  关闭URL请求
        curl_close($curl);
        //  返回
        return $exec;
    }

    /**
     * 记录错误信息
     * @param $error
     * @return mixed
     */
    public function setError ($error)
    {
        $this->error['error']       =   $error;
        $this->error['info']        =   curl_getinfo( $this->curl );
        return ;
    }

    /**
     * 获取错误信息
     * @return mixed
     */
    public function getError ()
    {
        return $this->error;
    }
}