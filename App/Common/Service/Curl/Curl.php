<?php
namespace Common\Service\Curl;

use Common\Provide\Singleton;
use Common\Service\Traits\__CallStatic;

/**
 * Class Curl
 * @package Common\Service\Curl
 * @method static post ($url, $data=[], $return=true)
 * @method static get ($url, $data=[], $return=true)
 * @method static getError ()
 */
class Curl
{
    use __CallStatic;

    public function getCalledAccessor()
    {
        // TODO: Implement getCalledAccessor() method.
        return Handle::getInstance();
    }
}