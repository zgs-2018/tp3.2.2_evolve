<?php
namespace Common\Service\Validate;

use Common\Provide\Singleton;

class Handle extends Singleton
{
    /**
     * 规则解析器
     *
     * @var \Common\Service\Validate\Resolve;
     */
    public $resolver;

    /**
     * @var \Common\Service\Validate\Validator;
     */
    public $validator;

    /**
     * 存储验证数据
     * @var array
     */
    protected $objects      =   [];

    /**
     * 存储验证规则
     * @var array
     */
    protected $regular      =   [];

    /**
     * 错误提示信息
     * @var array
     */
    protected $message      =   [];

    /**
     * 记录错误信息
     * @var
     */
    private $error;

    /**
     * @ 构造验证实例
     * @param array $params
     * @param array $regular
     * @param array|null $message
     * @return $this
     */
    public function make (array $params, array $regular, array $message=null)
    {
        //
        $this->objects          =   $params;
        // 设置验证规则
        $this->setRegular( $regular );
        // 记录错误提示信息
        $this->message          =   $message;
        return $this;
    }

    /**
     * @ 验证
     * @return bool
     */
    public function validate ()
    {
        //  验证器
        $this->validator        =   Validator::getInstance();
        //  验证规则
        $regular                =   $this->getRegular();
        //
        foreach ( $regular as $key => $value ) {
            //  当前参数
            $currentParam       =   exists_key( $key, $this->objects, 2 ) ? $this->objects[$key] : '';
            //  循环验证处理
            foreach ( $value as $op ) {
                if( !$this->validator->handle( $this, $op['method'], $currentParam, $op['params'] ) ){
                    // message
                    if( !($message=$this->getMessage($key, $op['method'])) ){
                        $message        =   $key . '~' . $op['method'];
                    }
                    // 记录错误信息、记录错误字段、记录错误操作
                    $record             =   [
                        'field' =>  $key, 'operate' => $op['method'], 'message' => $message
                    ];
                    $this->setError( $record );
                    // 中断返回
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 获取验证规则
     * @param $field
     * @return mixed|null
     */
    public function getRegular ($field=null)
    {
        if( is_null($field) )   return $this->regular;
        return exists_key( $field, $this->regular )
            ?   $this->regular[ $field ]
            :   null;
    }

    /**
     * @ 设置验证规则
     * @param $regular
     * @return string
     */
    public function setRegular ($regular)
    {
        return $this->regular = $this->resolve( $regular );
    }

    /**
     *  解析验证规则
     * @param $regular
     * @return string
     */
    public function resolve ($regular)
    {
        $this->resolver      =   new Resolve();

        return $this->resolver->parser($regular, array_keys( $this->objects ));
    }

    /**
     * @param null $field
     * @param null $operate
     * @return array|mixed
     */
    public function getMessage ($field=null, $operate=null)
    {
        $message            =   $this->message;
        // 获取所有信息
        if( is_null( $field ) || is_null($operate) )
            return $message;

        // 一维信息
        $key                =   $field . '~' . $operate;
        if( exists_key( $key, $message ) )
            return $message[ $key ];

        // 二维信息
        if( exists_key($field, $message) && exists_key( $operate, $message[$field] ) )
            return $message[$field][$operate];

        // 公共信息
        return $this->getPublicMessage( $operate, $field );
    }

    /**
     * @return mixed
     */
    public function getError ()
    {
        return $this->error;
    }
    /**
     * @param $error
     * @return mixed
     */
    public function setError ($error)
    {
        return ( $this->error=$error );
    }

    /**
     * @param null $field
     * @return array
     */
    public function getPublicMessage ($operate=null, $field=null)
    {
        $public             =   [
            'mobile'            =>  '手机号不合法',
            'email'             =>  '邮箱不合法',
            'required'          =>  ':field 不能为空',
            'between'           =>  ':field 不在区间之内',
            'number'            =>  ':field 不是数字类型',
            'in'                =>  ':field 不合法',
            'min'               =>  ':field 不满足长度最小限制',
            'max'               =>  ':field 不满足长度最大限制',
            'length'            =>  ':field 长度不合法',
            'eq'                =>  ':field 不匹配',
            'verify'            =>  ':field 不合法',
        ];

        if ( is_null($operate) ) return $public;

        if ( ! exists_key($operate, $public) )  return null;

        if( is_null($field) )   return $public[$operate];

        return str_replace( ':field', $field, $public[$operate] );
    }
}