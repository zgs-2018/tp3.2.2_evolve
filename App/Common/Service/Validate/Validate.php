<?php
namespace Common\Service\Validate;

use Common\Service\Service;
use Common\Service\Traits\__CallStatic;
use Common\Service\Traits\Yar;

/**
 * Class Validate
 * @package Common\Service\Validate
 * @method static make (array $params, array $regular, array $message=null)
 * @method validate ()
 * @method static getError ()
 */
class Validate
{
    use __CallStatic;

    public function getCalledAccessor()
    {
        // TODO: Implement registerHandle() method.
        return Handle::getInstance();
    }
}