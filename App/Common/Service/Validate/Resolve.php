<?php
namespace Common\Service\Validate;

class Resolve
{
    /**
     * 解析
     * @param $regular
     * @param null $refer
     * @return array
     */
    public function parser ($regular, $refer=null)
    {
        $parsed                 =   [];
        //
        if( is_null($refer) ){
            // 解析所有
            foreach ( $regular as $field => $fuzzy ){
                $parsed[ $field ]           =   $this->explicit( $fuzzy );
            }

        }else{
            //
            foreach ( $regular as $field => $fuzzy ){
                $parsed[ $field ]           =   exists_key( $field, $regular )
                    ?   $this->explicit( $fuzzy )
                    :   null;
            }
        }

        return $parsed;
    }

    /**
     * @param $objects
     * @param string $delimiter
     * @return array
     */
    public function explode ($objects ,$delimiter='|')
    {
        return explode( $delimiter, $objects );
    }

    /**
     *  获取明确
     * @param $fuzzy
     * @return array
     */
    public function explicit ($fuzzy)
    {
        $explode                =   $this->explode( $fuzzy );
        $explicit               =   [];
        foreach ( $explode as $field => $rule ){
            $operate            =   $this->parseOperate($rule);
            $explicit[ $operate['method'] ] =   $this->parseOperate($rule);
        }

        return $explicit;
    }

    /**
     * 分离操作、参数
     * @param $rule
     * @return array
     */
    public function parseOperate ($rule)
    {
        $one                    =   $this->explode($rule, ':');

        return [
            'method'        =>  $one[0],
            'params'        =>  exists_key('1', $one) ? $this->explode($one[1], ',') : null,
        ];
    }
}