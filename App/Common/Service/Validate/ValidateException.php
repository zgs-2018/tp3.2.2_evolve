<?php
namespace Common\Service\Validate;

use Think\Exception;
use Throwable;

class ValidateException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}