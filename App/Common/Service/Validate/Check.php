<?php
namespace Common\Service\Validate;

use Common\Service\Service;
use Common\Service\Traits\__CallStatic;

/**
 * Class Check
 * @package Common\Service\Validate
 * @method static mobile ($mobile)
 * @method static email ($email)
 * @method static length ($str,$length)
 */
class Check
{
    use __CallStatic;

    public function getCalledAccessor()
    {
        // TODO: Implement registerHandle() method.
        return Validator::getInstance();
    }
}