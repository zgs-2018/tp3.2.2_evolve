<?php
namespace Common\Service\Validate;

use Common\Provide\Singleton;
use Common\Service\Verify\Verify;

class Validator extends Singleton
{
    /**
     * @var \Common\Service\Validate\Handle;
     */
    protected $handle;

    public function handle ($handle, $method, $value, $params=null)
    {
        $this->handle       =   $handle;
        if( method_exists( $this, $method ) ){
            return $this->{$method}($value, $params);
        }
        E( '验证规则不存在：' . $method );
    }

    public function required ($value, $params=null)
    {
        return ( $value !== '' );
    }

    public function mobile ($value, $params=null)
    {
        $pattern            =   '/^(0086|\+86)?(1[345678][0-9]{9})$/';
        return  pattern( $pattern, $value );
    }

    public function cmobile ($value, $params=null)
    {
        $pattern            =   '/^[0-9]{6,14}$/';
        return pattern( $pattern, $value );
    }

    public function email ($value, $params=null)
    {
        $pattern            =   '/^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
        return pattern( $pattern, $value );
    }

    public function number ($value, $params=null)
    {
        if( !$this->required( $value ) ) return true;

        if( is_numeric( $value ) )  return true;
        return false;
    }

    public function in ($value, $params=null)
    {
        if( !$this->required( $value ) ) return true;

        return in_array( $value, $params );
    }

    public function between ($value, $params=null)
    {
        if( !$this->required( $value ) ) return true;

        $min                =   exists_key( 0, $params ) ? (int)$params[0] : PHP_INT_MIN;
        $max                =   exists_key( 1, $params ) ? (int)$params[1] : PHP_INT_MAX;
        return (
            ((int)$value>=$min) && ((int)$value<=$max)
        );
    }

    public function min ($value, $params=null)
    {
        if( !$this->required( $value ) ) return true;

        return ( is_array( $value ) ? count($value) : strlen($value) ) >= $params[0];
    }

    public function max ( $value, $params=null )
    {
        if (!$this->required($value)) return true;

        return (is_array($value) ? count($value) : strlen($value)) <= $params[0];
    }

    public function length ($value, $params=null)
    {
        if (!$this->required($value)) return true;
        $length                 =   is_array($params)
            ?   $params[0]
            :   $params;
        return ( mb_strlen( $value, 'UTF8' )==$length );
    }

    public function eq ($value, $params=null)
    {
        if (!$this->required($value)) return true;
        return $params[0] == $value;
    }

    public function verify ($value, $params=null)
    {
        if (!$this->required($value)) return true;
        $sign               =   $params[0];
        $target             =   $params[1];
        return Verify::check( $sign, $target, $value );
    }
}