<?php
namespace Common\Service\Tree;

use Common\Service\Traits\__CallStatic;

/**
 * Class Tree
 * @package Common\Service\Tree
 * @method static init (array $data, $id='id', $pid='pid', $child='child', $nameKey='name', $levelKey='level')
 * @method static trees ($id, $withSelf=false, $recursion=true)
 * @method static lists ($id, $explicit='----', $withSelf=true)
 * @method static parent ($id,bool $withSelf=false, $recursion=true)
 */
class Tree
{
    use __CallStatic;

    public function getCalledAccessor()
    {
        // TODO: Implement getCalledAccessor() method.
        return  Handle::getInstance();
    }
}