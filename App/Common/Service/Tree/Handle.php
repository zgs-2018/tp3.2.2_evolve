<?php
namespace Common\Service\Tree;

use Common\Provide\Singleton;

class Handle extends Singleton
{
    /**
     * 操作数据
     * @var array
     */
    public $data = false;

    /**
     * 主键名
     * @var string
     */
    public $primaryKey;

    /**
     * 父类键名
     * @var string
     */
    public $parentKey;

    /**
     * 子元素集键名
     * @var string
     */
    public $childKey;

    /**
     * @var string
     */
    public $nameKey;

    /**
     * @var string
     */
    public $levelKey;

    /**
     * @param array $data
     * @param string $id
     * @param string $pid
     * @param string $child
     * @param string $nameKey
     * @param string $levelKey
     * @return $this
     */
    public function init (array $data, $id='id', $pid='pid', $child='child', $nameKey='name', $levelKey='level')
    {
        $this->primaryKey       =   $id;
        $this->parentKey        =   $pid;
        $this->childKey         =   $child;
        $this->nameKey          =   $nameKey;
        $this->levelKey         =   $levelKey;
        $this->setData( $data );
        return $this;
    }

    /**
     * 获取 列表格式数据
     * @param $id
     * @return array|bool|mixed
     */
    public function lists ($id, $explicit='----', $withSelf=true)
    {
        //  声明结果集
        $result                 =   [];
        if( $withSelf ){
            //  记录自身
            $self                       =   $this->getData( $id );
            if( $self ){
                $self[$this->levelKey]  =   0;
                $result[$id]            =   $self;
            }
        }
        //  处理
        $this->trees2lists(
            $this->child($id, false, true), $result, 1
        );
        if( $explicit ){
            $result                 =   array_map( function($value) use($explicit){
                $value[ $this->nameKey ]        =   str_repeat( $explicit, $value[ $this->levelKey ] ) . $value[ $this->nameKey ];
                return $value;
            }, $result );
        }
        return $result;
    }

    /**
     * @param $id
     * @return array|bool|mixed
     */
    public function trees ($id, $withSelf=false, $recursion=true)
    {
        return $this->child($id, $withSelf, $recursion );
    }

    /**
     * @param $trees
     * @param $result
     * @param int $level
     * @return array
     */
    public function trees2lists ($trees, &$result, $level=1)
    {
        foreach ($trees as $value){
            //  记录等级
            $value[ $this->levelKey ]       =   $level;
            //  获取子元素
            $child                          =   exists_key( $this->childKey, $value )
                ?   $value[ $this->childKey ]
                :   false;
            //  摧毁当前元素子元素
             unset( $value[ $this->childKey ] );
            //  记录当前元素
            $result[ $value[$this->primaryKey] ]                   =   $value;
            //  递归
            if( $child ){
                $this->trees2lists( $child, $result, $level+1 );
            }
            continue;
        }
        return $result;
    }

    /**
     *  获取子元素
     * @param $id
     * @param bool $withSelf
     * @return array|bool|mixed
     */
    public function child ($id, $withSelf=false, $recursion=true, $data=false)
    {
        //  初始化获取数据
        if( $data === false ){
            $data               =   $this->getData();
        }
        //  判断是否有数据
        if( ! $data )   return [];
        //  自身元素储存
        $self               =   [];
        //  子元素储存
        $child              =   [];
        //  遍历数据
        foreach ($data as $key => $value) {
            //  记录自身元素数据
            if( $value[ $this->primaryKey ] == $id ){
                $self           =   $value;
            }elseif( $value[ $this->parentKey ] == $id ){
                //  记录当前元素
                $current                            =   $value;
                //  是否递归获取当前元素子元素
                if( $recursion ){
                    unset( $data[ $key ] );
                    $current[ $this->childKey ]     =   $this->child( $value['id'], false, true, $data );
                }
                $child[]        =   $current;
            }
            continue;
        }
        //
        if( $withSelf ){
            $self[ $this->childKey ]      =   $child;
            return $self;
        }
        return $child;
    }

    /**
     * @param $id
     * @param bool $withSelf
     * @param bool $recursion
     * @return array
     */
    public function parent ($id, $withSelf=false, $recursion=true)
    {
        //  声明结果集
        $result             =   [];
        //  处理
        $this->getParent( $id, $result, $withSelf, $recursion );
        return $result;
    }

    /**
     * @param $id
     * @param $result
     * @param $withSelf
     * @param $recursion
     * @return bool|mixed|void
     */
    public function getParent ($id, &$result, $withSelf, $recursion)
    {
        //  父级id
        $parentId           =    ( $self = $this->getData( $id ) )
            ?   $self[ $this->parentKey ]
            :   false;
        if( $parentId===false || $parentId === 0 ) return $parentId;
        //  自身
        $withSelf && ($result[]=$self);
        //  记录
        ( $parentData=$this->getData( $parentId ) ) && array_push( $result, $parentData );
        //  是否递归
        if( $recursion )
            $this->getParent( $parentId, $result, false, true );
        return ;
    }

    /**
     * @param $origin
     * @return array
     */
    public function setData ($origin)
    {
        $data               =   maps($origin,$this->primaryKey );
        $this->data         =   $data;
        return $data;
    }

    /**
     * 获取原始数据
     * @return array
     */
    public function getData ($id=null)
    {
        ( $this->data===false ) && E('Tree error: 为初始化数据', 404);
        return $id===null
            ?   $this->data
            :   ( exists_key($id, $this->data) ? $this->data[$id] : [] );
    }
}