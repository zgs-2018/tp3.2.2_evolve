<?php
namespace Common\Provide;

class Singleton
{
    /**
     *  实例库
     * @var array
     */
    public static $instances  =   [];

    /**
     * @获取实例
     * @param null $name
     * @return static
     */
    public static function getInstance ($name=null)
    {
        $name               =  is_null($name)
            ?   get_called_class()
            :   $name;
        if( !isset( self::$instances[$name] ) ){
            self::$instances[$name] = new $name();
        }
        return self::$instances[$name];
    }
}