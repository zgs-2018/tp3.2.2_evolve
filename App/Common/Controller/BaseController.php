<?php

namespace Common\Controller;

use Common\Controller\Base\Spread;
use Common\Service\Validate\Handle;
use Think\Controller;

class BaseController extends Controller
{
    use Spread;

    protected $error;

    public function __construct()
    {
        parent::__construct();
    }

    public function validate ( $params, array $regular, array $message=null  )
    {
        $validate               =   new Handle();

        $validate               =   $validate->make( $params, $regular, $message );

        if( $validate->validate() === false ){
            $this->setError( $validate->getError() );
            return false;
        }
        return true;
    }

    /**
     * @ 获取错误记录
     * @return mixed
     */
    protected function getError ()
    {
        return $this->error;
    }

    /**
     * @ 记录错误信息
     * @param $error
     * @return mixed
     */
    protected function setError ($error)
    {
        return ( $this->error=$error );
    }

}