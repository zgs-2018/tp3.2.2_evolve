<?php
namespace Common\Controller\Base;

use Common\Model\KeywordsModel;
use Common\Model\SpreadModel;

trait Spread
{
    /**
     * 根据route设置seo数据
     * @param $route
     * @return bool
     */
    public function setSeoDataByRoute ($route)
    {
        $data           =   SpreadModel::getDataByRoute( $route );
        if( $data ){
            $this->assign([
                'title'         =>  $data['title'],
                'keywords'      =>  $data['keywords'],
                'description'   =>  $data['desc'],
            ]);
            return true;
        }
        return false;
    }

    /**
     * 获取栏目
     * @return array|mixed
     */
    public function getColumns ()
    {
        $data           =   SpreadModel::getAllOriginData();
        $columns        =   array_filter($data, function ($v){
            return $v['is_column'] == 1;
        });

        return $columns;
    }
}