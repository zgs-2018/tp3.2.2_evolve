<?php
namespace Common\Model;

class SpreadModel extends OEModel
{
    protected $tableName                =   'web_spread';

    protected static $cacheKey          =   'data.spread';

    /**
     * @param $sign
     * @return mixed
     */
    public static function getDataByRoute ($route)
    {
        $allData                =   static::getAllOriginData();
        $data                   =   exists_key($route, $allData)
            ?   $allData[$route]
            :   [];
        if( $data )
            $data['keywords']   =   static::getKeysByIds( $data['keywords_ids'] );
        return $data;
    }

    /**
     * @param $ids
     * @return array|string
     */
    public static function getKeysByIds ($ids)
    {
        if( !$ids ) return [];
        $arrIds                 =   is_array($ids) ?   $ids :   explode( ',', $ids );
        //  获取关键词数据
        $keywords               =   KeywordsModel::getAllOriginData();
        //  循环取值
        $keys                   =   [];
        foreach ($arrIds as $kid){
            if( exists_key($kid, $keywords) )
                $keys[]     =   $keywords[ $kid ]['name'];
        }
        return implode(',', $keys);
    }

    /**
     * 原始数据
     * @return array|mixed
     */
    public static function getAllOriginData ()
    {
        //  是否有缓存
        if( C('MODEL_CACHE') && S( static::$cacheKey ) )
            return S( static::$cacheKey );
        $self                       =   new static();
        //  获取程序数据
        $appData                    =   AppsModel::getAppDataByAppid();
        $type               =   $appPriId   =   $appData['id'];
        //  获取推广原始数据
        $origin                 =   $self->field(true)
            ->where(['type'=>['eq',$type]])
            ->order('weigh desc')
            ->select() ?: [];
        //  映射处理
        $maps                       =   maps($origin, 'route');

        //  缓存
        $maps && S( static::$cacheKey, $maps );
        return $maps;
    }
}