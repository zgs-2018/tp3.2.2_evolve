<?php
namespace Common\Model;


class OEModel extends MacroModel
{
    protected $dbName                   =   'oe';

    protected $tablePrefix              =   'oe_';

    protected $_scope                       =   [
        'valid'                        =>  [
            'where'         =>  ['status'=>1],
        ],
    ];
}