<?php
namespace Common\Model;

class ArticlesModel extends OEModel
{
    protected $tableName                =   'articles';

    /**
     * 获取所有文章分类
     * @return bool
     */
    public static function category ()
    {
        $categories         =   TagsModel::byKeywords( 'article_type', true,'' );
        return $categories['child'] ?? [];
    }
}