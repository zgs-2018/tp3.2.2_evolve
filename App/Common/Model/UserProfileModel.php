<?php
namespace Common\Model;

class UserProfileModel extends OEModel
{
    protected $tableName                    =   'users_profile';

    protected $pk                           =   'user_id';

    protected $_link                        =   [
        'user'                          =>  [
            'mapping_type'          =>  self::BELONGS_TO,
            'class_name'            =>  'User',
            'foreign_key'           =>  'user_id',
        ],
    ];

    protected $allowFields                  =   [
        'nickname', 'realname', 'email', 'head_img', 'age', 'sex', 'city', 'address'
    ];

    /**
     * @ 刷新登录状态
     * @param array $profile
     * @return bool
     */
    public function refreshLoginInfo (array $profile)
    {
        $profileModel           =   new UserProfileModel();

        return $profileModel->where(['user_id'=>['eq',$profile['user_id']]])
            ->save([
                'last_login_ip'     =>  get_client_ip(),
                'last_login_time'   =>  date('Y-m-d H:i:s', time()),
                'last_login_appid'  =>  $profile['last_login_appid'],
            ]);
    }

    /**
     * @param $sign
     * @param $username
     * @return bool
     */
    public function exists ($sign, $username)
    {
        //  条件字段判断
        if( !in_array( $sign, ['email'] ) )
            return false;
        $exact              =   $this->field(true)
            ->where([
                $sign       =>  ['eq', $username],
            ])
            ->find();

        return $exact ?: false;
    }

    protected function _before_update(&$data, $options)
    {
        $data['updated_at']     =   date( 'Y-m-d H:i:s', time() );
    }
}