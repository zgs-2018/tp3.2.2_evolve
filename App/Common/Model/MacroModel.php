<?php
namespace Common\Model;

use Think\Model\RelationModel;

abstract class MacroModel extends RelationModel
{
    /**
     * 缓存标识
     * @var
     */
    protected static $cacheKey;

    /**
     * @param array $where
     * @param array $fields
     * @param array $orderBy
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function selectLists (array $where, $page=1, $pageSize=20, $fields=false, $orderBy=false )
    {
        //  设置查询字段
        $this->setSelectFields( $fields );
        //  设置排序方式
        $this->setSelectOrder( $orderBy );
        return $this->buildQuery($where)
            ->page( $page )
            ->limit( $pageSize )
            ->select() ?: [];
    }

    /**
     * @param array $where
     * @return int
     */
    public function selectCount (array $where)
    {
        $count          =   $this->buildQuery($where)->count();
        return $count ?   (int)$count :   0;
    }

    /**
     * @ 构造查询语句
     * @param array $where
     * @return \Think\Model
     */
    protected function buildQuery ($where=[])
    {
        return  $this->where($where);
    }

    /**
     * @param $fields
     * @return \Think\Model
     */
    protected function setSelectFields ($fields)
    {
        $default                =   [true,false];
        //  默认值
        if( $fields === false )
            return $this->field( $default[0], $default[1] );
        //  字符串
        if( is_string( $fields ) )
            return $this->field( $fields );
        //  二维数组
        if( is_array( $fields[0] ) )
            return $this->field( ...$fields );
        //  一维数组
        return $this->field( $fields );
    }

    /**
     * @param $order
     * @return mixed
     */
    protected function setSelectOrder ($order)
    {
        $default                =   ['id', 'desc'];
        //  默认值
        if( $order === false )
            return $this->order( "{$default[0]} {$default[1]}" );
        //  字符串
        if( is_string( $order ) )
            return $this->order( $order );
        //  数组
        if ( exists_key(1, $order) ){
            return $this->order( "{$order[0]} {$order[1]}" );
        }
        return $this->order( $order[0] );
    }
}