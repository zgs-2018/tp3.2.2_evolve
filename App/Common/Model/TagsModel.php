<?php
namespace Common\Model;

use Common\Service\Tree\Tree;

class TagsModel extends OEModel
{
    protected $tableName                =   'tags';

    protected static $cacheKey             =   'data.tags';

    /**
     * @param $keywords
     * @param bool $data
     * @return bool|mixed
     */
    public static function getItemByKeywords ($keywords, $data=false)
    {
        //  获取原始数据
        $origin             =   $data ?: static::getAllOriginData();
        foreach ($origin as $value){
            if( $value['keywords'] == $keywords )
                return $value;
        }
        return false;
    }

    /**
     * 通过ID  获取元素
     * @param $id
     * @param bool $data
     * @return bool|mixed
     */
    public static function getItemById ($id,$data=false)
    {
        //  获取原始数据
        $origin             =   $data ?: static::getAllOriginData();
        return exists_key($id,$origin) ? $origin[$id] : false;
    }

    /**
     * 通过关键词 获取列表
     * @param null $keywords
     * @param bool $tree
     * @param string $explicit
     * @return bool
     */
    public static function byKeywords ($keywords=null,$tree=true, $explicit='' )
    {
        //  获取所有数据
        $originData             =   static::getAllOriginData();
        //  初始化tree
        Tree::init($originData);
        //
        if( ! $keywords )
            return $tree ?   Tree::trees(0,true) :   Tree::lists(0,'',true);
        //  对象
        $current                =   static::getItemByKeywords($keywords, $originData) ?: false;
        //
        if( $current )
            return $tree ? Tree::trees( $current['id'],true ) : Tree::lists( $current['id'], $explicit,true );
        return false;
    }

    /**
     * 通过id 获取列表
     * @param null $id
     * @param bool $tree
     * @param string $explicit
     * @return mixed
     */
    public static function byId ($id=null,$tree=true,$explicit='')
    {
        //  获取所有数据
        $origin                 =   static::getAllOriginData();
        //  初始化Tree
        Tree::init($origin);
        if( ! $id )
            return $tree ?   Tree::trees(0,true) :   Tree::lists(0,'',true);
        //  操作对象
        return $tree ? Tree::trees( $id,true ) : Tree::lists( $id, $explicit,true );
    }

    /**
     * @ 获取所有原始数据
     * @return array|mixed
     */
    public static function getAllOriginData ()
    {
        //  默认读取缓存
        if( C('MODEL_CACHE') && S( static::$cacheKey ) )
            return S( static::$cacheKey );
        //  数据查询
        $data           =   (new static())->field(true)
            ->where(['status'=>['eq',1]])
            ->select() ?: [];
        //  映射
        $maps           =   maps( $data, 'id' );
        //  缓存
        $maps && S( static::$cacheKey, $maps );
        return $maps;
    }
}