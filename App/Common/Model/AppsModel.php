<?php
namespace Common\Model;

class AppsModel extends OEModel
{
    protected $tableName                =   'apps';

    protected static $cacheKey          =   'data.apps';

    /**
     * 通过appid获取app数据
     * @param null $appid
     * @return array|bool|mixed
     */
    public static function getAppDataByAppid ($appid=null)
    {
        //  程序appid
        $appid                  =   $appid ?: C('APP_ID');
        if( !$appid )   return false;
        //  原始数据
        $origin                 =   static::getAllOriginData();
        //  程序数据
        return exists_key($appid, $origin)
            ?   $origin[ $appid ]
            :   [];
    }

    /**
     * 原始数据
     * @return array|mixed
     */
    public static function getAllOriginData ()
    {
        //  是否有缓存
        if( C('MODEL_CACHE') && S( static::$cacheKey ) )
            return S( static::$cacheKey );
        //  原始数据
        $origin                 =   (new static())->field(true)
            ->where(['status'=>['eq',1]])
            ->select();
        //  映射处理
        $maps                   =   maps($origin, 'appid');
        //  缓存
        $maps && S( static::$cacheKey, $maps );
        return $maps;
    }
}