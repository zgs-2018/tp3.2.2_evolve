<?php
namespace Common\Model;

class SguModel extends OEModel
{
    protected $tableName            =   'sgu';

    protected static $cacheKey      =   'data.sgu';

    protected static $relationsCacheKey =   'data.sgu.majorCateRelations';

    protected $_link                =   [
        'School'        =>  [
            'mapping_type'          =>  self::HAS_ONE,
            'class_name'            =>  'Schools',      //  目标模型名
            'mapping_name'          =>  'school_info',       //  数据键
            'foreign_key'           =>  'id',           //  目标模型关联字段
            'mapping_fields'        =>  true,
        ],
        'Majors'        =>  [
            'mapping_type'          =>  self::HAS_MANY,
            'class_name'            =>  'Major',
            'foreign_key'           =>  'school_id',
            'mapping_fields'        =>  true,
            'mapping_name'          =>  'majors',
            'mapping_limit'         =>  10,
            'mapping_order'         =>  'id asc',
        ],
    ];

    /**
     * sgu专业分类
     * @return bool
     */
    public static function majorCategory ()
    {
        $category       =   TagsModel::byKeywords( 'major_master_sgu', true, '' );
        return $category ? $category['child'] : [];
    }

    /**
     * 专业关联学校
     * @return array
     */
    public function sguMajorCateRelations ()
    {
        //  缓存默认缓存
        if( C('MODEL_CACHE') && S( static::$relationsCacheKey ) )
            return S( static::$relationsCacheKey );
        //  获取sgu专业分类
        $category       =   maps(static::majorCategory(),'id') ;
        $category_ids   =   array_column( $category, 'id' );
        //  获取sgu数据
        $origin         =   static::getAllOriginData();
        //  获取所有包含专业分类的专业
        $majorsModel    =   new MajorsModel();
        $majors         =   $majorsModel->field('id,name_cn,school_id,cate_id')
            ->where(['status'=>['eq',1],'cate_id'=>['in',$category_ids]])
            ->select() ?: [];
        //  关联分类
        foreach ($majors as $major){
            $category[ $major['cate_id'] ]['schools'][ $major['school_id'] ]     =   $origin[ $major['school_id'] ] ;
        }
        //  缓存
        S( static::$relationsCacheKey, $category );
        return $category;
    }

    /**
     * 原始数据
     * @param array $where
     * @param null $fields
     * @return array|mixed
     */
    public static function getAllOriginData ($where=[], $fields=null)
    {
        //  默认读取缓存
        if( C('MODEL_CACHE') && S( static::$cacheKey ) )
            return S( static::$cacheKey );
        //  数据查询
        $_where         =   ['sgu.status'=>['eq',1], ['s.status'=>['eq',1]]];
        $_fields        =   'sgu.id,sgu.id_school,sgu.remark_text,s.name_cn,s.name_jp,s.name_en,s.style_logo,s.sm_logo,s.long_logo';
        $where          =   array_merge( $_where, $where );
        $fields         =   $fields ?: $_fields;
        $data           =   (new static())->alias('sgu')
            ->field($fields)
            ->join('LEFT JOIN __SCHOOLS__ s ON sgu.id_school = s.id')
            ->where($where)
            ->order('weigh desc')
            ->select() ?: [];
        //  映射
        $maps           =   maps( $data, 'id_school' );
        //  缓存
        $maps && S( static::$cacheKey, $maps );
        return $maps;
    }

}