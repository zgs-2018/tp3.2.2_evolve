<?php
namespace Common\Model;

class KeywordsModel extends OEModel
{
    protected $tableName                =   'keywords';

    protected static $cacheKey          =   'data.keywords';

    /**
     * @return mixed
     */
    public static function getAllOriginData ()
    {
        //  是否有缓存
        if( C('MODEL_CACHE') && S( static::$cacheKey ) )
            return S( static::$cacheKey );
        //  提取数据
        $origin                 =   (new self())->field(true)
            ->where(['status'=>['eq',1]])
            ->select();
        //  映射转换
        $maps                   =   maps($origin, 'id');
        //  缓存
        $maps && S( static::$cacheKey, $maps );
        return $maps;
    }
}