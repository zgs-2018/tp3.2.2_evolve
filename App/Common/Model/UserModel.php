<?php
namespace Common\Model;

use Common\Service\Auth\Auth;
use Common\Service\Common\Common;
use Common\Service\Str\Str;
use Think\Exception;

class UserModel extends OEModel
{
    protected $tableName                    =   'users';

    protected $pk                           =   'user_id';

    protected $_link                        =   [
        'profile'                   =>  [
            'mapping_type'          =>  self::HAS_ONE,
            'class_name'            =>  'UserProfile',
            'foreign_key'           =>  'user_id',
            'mapping_fields'        =>  true,
        ],
    ];

    /**
     * @ 授权验证
     * @param $sign
     * @param $username
     * @param $password
     * @return bool
     */
    public function authorize ($sign, $username, $password)
    {
        $exact              =   $this->exists( $sign, $username );
        if(
            $exact
            && ( $exact[$sign] == $username )
            &&  Str::checkPassword( $password, $exact['password'] )
        ){
            return $exact;
        }

        return false;
    }

    /**
     * @ 用户是否存在
     * @param $sign
     * @param $username
     * @return bool
     */
    public function exists ($sign, $username)
    {
        //  条件字段判断
        if( !in_array( $sign, $this->getDbFields() ) )
            return false;
        $exact              =   $this->relation(true)
            ->where([
                $sign       =>  ['eq', $username],
                'status'    =>  ['eq', 1],
            ])
            ->find();

        return $exact ?: false;
    }

    /**
     * @param $data
     * @return bool
     */
    public function register ($data)
    {
        try{
            //  构造数据
            $data['openid']                     =   Common::UUID();
            $data['password']                   =   Str::buildPassword($data['mobile']);
            $data['created_at']                 =   date('Y-m-d H:i:s');
            //  开启事务
            $this->startTrans();
            //  入库
            $this->add($data);
            //  ID
            $user_id                            =   $this->getLastInsID();
            //  添加附表
            $profile['user_id']                 =   $user_id;
            $profile['ssid']                    =   Str::ssid($user_id);
            $profile['nickname']                =   '学员_' . substr($data['mobile'], -4);
            $profile['created_at']              =   date('Y-m-d H:i:s');
            //
            (new UserProfileModel())->add($profile);
            //  提交事务
            $this->commit();
            return $this->relation(true)->field(true)->find($user_id);
        }catch (Exception $e){
            $this->rollback();
            return false;
        }
    }

    /**
     *  当前用户信息
     * @return bool
     */
    public function current ()
    {
        //  当前openId
        if( !( $openid = Auth::openID() ) )  return false;

        return $this->field('password,remember_token',true)
            ->relation(true)
            ->where(['openid'=>['eq',$openid]])
            ->find() ?: true;
    }
}