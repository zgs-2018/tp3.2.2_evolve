<?php
namespace Common\Model;

class RecruitsModel extends OEModel
{
    protected $tableName                =   'recruit';

    /**
     * 招聘分类
     * @return array
     */
    public static function category ()
    {
        $categories         =   TagsModel::byKeywords( 'recruit_cate', true,'' );
        return $categories['child'] ?? [];
    }
}