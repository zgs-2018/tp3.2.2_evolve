<?php
namespace Common\Model;

class CrmModel extends MacroModel
{
    protected $dbName                   =   'crm';

    protected $tablePrefix              =   'crm_';

    protected $_scope                       =   [
        'valid'                        =>  [
            'where'         =>  ['status'=>1],
        ],
    ];
}