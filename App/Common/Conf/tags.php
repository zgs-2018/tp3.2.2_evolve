<?php

return [
    'app_init'          =>  [
        'Common\Behavior\ShutDownBehavior',
    ],

    'path_info'         =>  [],
    'app_begin'         =>  [],
    'action_begin'      =>  [
        'Common\Behavior\InterceptAuthBehavior',
        'Common\Behavior\HeaderSetBehavior',
        'Common\Behavior\AuthorizationBehavior',
        'Common\Behavior\ForbiddenAuthorizeBehavior',
    ],
    'view_begin'        =>  [],
    'view_parse'        =>  [],
    'template_filter'   =>  [],
    'view_filter'       =>  [
        'Behavior\TokenBuildBehavior',
        'Common\Behavior\BuildAccessTokenBehavior',
    ],

    'view_end'          =>  [],
    'action_end'        =>  [],
    'app_end'           =>  [],
];