<?php

// +----------------------------------------------------------------------
// | 路由配置 <测试路由> ： 规则路由、静态路由、需授权路由
// +----------------------------------------------------------------------

return [
    'URL_ROUTE_RULES'       =>  [
        'api/test'              =>  'Test/Index/api',
        'ajax'                  =>  'Test/Index/ajax',
    ],

    'URL_MAP_RULES'         =>  [
        'test'                  =>  'Test/Index/debug',
        'center'                =>  'Test/Index/center',
        'fb'                    =>  'Test/Index/forbidden',
    ],

    'authorization'         =>  [
        'center'
    ],

    'forbidden'             =>  [
        'fb'
    ],
];