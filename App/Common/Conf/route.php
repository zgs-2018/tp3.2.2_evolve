<?php

// +----------------------------------------------------------------------
// | 路由配置 ： 规则路由、静态路由、需授权路由
// +----------------------------------------------------------------------

return [
    'URL_ROUTE_RULES'       =>  array_merge(
        load_config( CONF_PATH . 'Route/web.php' ) ,
        load_config( CONF_PATH . 'Route/api.php' )
    ),

    'URL_MAP_RULES'         =>  load_config( CONF_PATH . 'Route/map.php' ),

    'authorization'         =>  load_config( CONF_PATH . 'Route/authorization.php' ),

    'forbidden'             =>  load_config( CONF_PATH . 'Route/forbidden.php' ),
];