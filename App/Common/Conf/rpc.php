<?php

use \Yaconf as Yc;

return [
    'RPC_MAP'       =>  Yc::get('macro.wheel.server'),

    'RPC_ADDRESS'   =>  Yc::get('macro.wheel.domain'),
];