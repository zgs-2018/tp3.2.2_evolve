<?php
namespace Common\Behavior;

use Common\Service\Auth\Auth;
use Think\Behavior;

class ForbiddenAuthorizeBehavior extends Behavior
{
    public function run(&$params)
    {
        // 授权状态下禁止请求
        if( Auth::isLoginLocal() && in_array( $_SERVER['PATH_INFO'], C('forbidden') )  ){
            // 接口请求
            if( IS_AJAX ){
                $result             =   [
                    'result'            =>  false,
                    'status'            =>  403,
                    'error'             =>  'Authorized !'
                ];
                Ajax( $result );
            }
            // 传统请求
            redirect('/');
        }
        return ;
    }
}