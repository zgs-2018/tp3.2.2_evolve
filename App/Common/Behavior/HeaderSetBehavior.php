<?php
namespace Common\Behavior;

use Common\Service\Auth\Auth;
use Common\Service\Header\Header;
use Think\Behavior;

class HeaderSetBehavior extends Behavior
{
    public function run(&$params)
    {
        // TODO: Implement run() method.

        // 删除cookie thinkphp_show_page_trace
        cookie('?thinkphp_show_page_trace') && cookie( 'thinkphp_show_page_trace', null );
        // X-Powered-By
        Header::set( 'X-Powered-By', 'OE.luke', true );
        // X-Open-ID
        Auth::isLoginLocal() && Header::set( 'X-OpenID', Auth::openID() );
        // X-App-ID
        // Header::set( 'X-AppID', C('APP_ID') );
    }
}