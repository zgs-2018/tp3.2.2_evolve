<?php
namespace Common\Behavior;

use Common\Service\Auth\Auth;
use Think\Behavior;

class InterceptAuthBehavior extends Behavior
{
    public function run(&$params)
    {
        // 已经登录
        if( Auth::status() ){
            // 刷新票有效期
            Auth::refreshTicketExpire();
            return true;
        }
        // 未登录
        //      摧毁遗留登录信息
        Auth::isLoginLocal() && Auth::localLogout();
        //      是否有授权token
        if( ( $payload=Auth::getToken() ) && Auth::isMineToken($payload) ) {
            Auth::login( $payload );
            redirect('/');
        }

        return true;
    }
}