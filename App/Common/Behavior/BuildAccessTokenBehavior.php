<?php
namespace Common\Behavior;

use Common\Service\Auth\Auth;
use Common\Service\JWT\JWT;
use Think\Behavior;

class BuildAccessTokenBehavior extends Behavior
{
    protected $exp          =   1200;

    public function run(&$content)
    {
        if( !IS_AJAX && C('ACCESS_ON') && $content ) {
            //  创建token
            $access_token           =   $this->buildAccessToken();
            if( !$access_token ) return ;
            //  渲染
            $content        =   $this->render( $access_token, $content );
        }
        return true;
    }

    /**
     * 生成 access_token
     * @return mixed
     */
    protected function buildAccessToken ()
    {
        //  token 是否存在
        $claims                 =   $this->buildClaims();
        return JWT::createToken($claims);
    }

    /**
     * 构造html
     * @param $access_token
     * @param $content
     * @return mixed
     */
    protected function render ($access_token, $content)
    {
        $meta_html = "\r\n".'<meta name="ACCESS_TOKEN" content="'.$access_token.'" />';
        return str_ireplace('</head>',$meta_html . '</head>',$content);
    }

    /**
     * 传递信息
     * @return array
     */
    protected function buildClaims ()
    {
        return [
            'iss'           =>  C('SITE_URL'),
            'exp'           =>  time() + $this->exp,

            'ticket'        =>  Auth::getTicket() ?: false,
        ];
    }
}