<?php
namespace Common\Behavior;

use Common\Service\Auth\Auth;
use Common\Service\Common\Common;
use Common\Service\Header\Header;
use Common\Service\JWT\JWT;
use Think\Behavior;

class AuthorizationBehavior extends Behavior
{
    public function run(&$params)
    {
        // 无需验证全局授权、拦截行为已经判断过
        if( $this->needAuthorize( strtolower($_SERVER['PATH_INFO']) ) ){
            if( !IS_AJAX ){
                $certificate            =   static::createCertificate();
                Header::location( C('PASSPORT.url') . '?certificate=' . $certificate );
            }else{
                $result     =   [
                    'result'            =>  false,
                    'status'            =>  403,
                    'error'             =>  'Access Deny !',
                    'message'           =>  '',
                    'data'              =>  [],
                ];
                Ajax( $result );
            }
        }
        return true;
    }

    /**
     * 需要验证
     * @param $uri
     * @return bool
     */
    protected function needAuthorize ($uri)
    {
        $uri            =   $uri ?: '/';
        if( Auth::isLoginLocal() ) {
            return false;
        } elseif ( C('authorization') === true ) {
            return true;
        } elseif ( is_array( C('authorization') ) ) {
            return in_array( $uri , C('authorization') ) && !Auth::isLoginLocal();
        }
        return false;
    }

    /**
     * 生成授权凭证
     * @return mixed
     */
    protected function createCertificate ()
    {
        $payload =  [
            'URL'               =>  C('SITE_URL') . DS . $_SERVER['PATH_INFO'],
            'ID'                =>  C('APP_ID'),
            'SECRET'            =>  C('APP_SECRET'),
            'SID'               =>  session_id(),
        ];

        return JWT::createToken( $payload );
    }
}