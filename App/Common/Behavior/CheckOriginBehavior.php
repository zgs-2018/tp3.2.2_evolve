<?php
namespace Common\Behavior;

use Think\Behavior;
use Think\Log;

class CheckOriginBehavior extends Behavior
{
    public function run(&$params)
    {
        //  来源判断
        if( ($http_origin=static::fetchOrigin()) === false )    return ;
        //  来源验证
        if( static::checkOrigin($http_origin,C('ALLOW_ORIGIN')) ){
            //  授权来源
            static::allowOrigin($http_origin);
            return ;
        } else{
            //  结束来源
            static::termination($http_origin,true);
        }
    }

    /**
     * 获取来源链接
     * @return bool
     */
    public static function fetchOrigin ()
    {
        if( exists_key( 'HTTP_ORIGIN', $_SERVER ) ){
            return $_SERVER[ 'HTTP_ORIGIN' ];
        } elseif( exists_key('HTTP_REFERER', $_SERVER) ) {
            return $_SERVER[ 'HTTP_REFERER' ];
        }
        return false;
    }

    /**
     * @ 检测来源是否合法
     * @param $main_domain
     * @param $allows
     * @return bool
     */
    public static function checkOrigin ($http_origin,$allows)
    {
        // 获取来源主域
        $resolved                   =   resolve_url($http_origin);
        $main_domain                =   $resolved['main'] . '.' . $resolved['postfix'];
        // 请求来源
        if( in_array(trim($main_domain), $allows) )
            return true;
        return false;
    }

    /**
     * @ 允许跨域操作
     * @param $domain
     * @param string $methods
     * @param string $credentials
     */
    public static function allowOrigin ($domain, $methods='GET',$headers='x-requested-with', $credentials='true')
    {
        header('Access-Control-Allow-Origin: ' . $domain);
        header('Access-Control-Allow-Methods: ' . $methods);
        header('Access-Control-Allow-Headers: ' . $headers);
        header('Access-Control-Allow-Credentials: ' . $credentials);
        defined('CROSS_DOMAIN') or define('CROSS_DOMAIN', true);
        return;
    }

    /**
     * @ 禁止跨域结束
     * @param $domain
     * @param bool $record
     */
    public static function termination ($domain, $record=false)
    {
        $data       =   [
            'result'        =>  403,
            'error'         =>  'Access Deny!',
        ];
        if( $record ){
            $message        =   "ORIGIN:'{$domain}' try to access {$_SERVER['REQUEST_URI']} !";
            Log::write( $message, Log::ERR, '', LOG_PATH . 'forbid_cross.log' );
        }
        Ajax( $data, 'jsonp' );
    }
}