<?php

if( !function_exists('') ){

}

if( !function_exists('str2array') ){

    /**
     * @
     * @param $str
     * @param string $delimiter
     * @return array|bool
     */
    function str2array($str,$delimiter=',')
    {
        if( is_array($str) )  return $str;
        if( !is_string($str) )  return false;
        if( $str === "" )   return [];
        return explode($delimiter, $str);
    }
}

if( !function_exists('exists_key') ){
    /**
     * @param $key
     * @param $array
     * @param int $mode 1.严谨模式 2.模糊模式
     * @return bool
     */
    function exists_key ($key, $array, $mode=1)
    {
        // 验证key合法度
        if( !is_array( $array ) )
            return false;
        $result             =   false;
        if(is_string($key) || is_int($key)){
            switch ($mode){
                case 1:
                    $result =   array_key_exists($key,$array) && $array[$key];
                    break;
                case 2:
                    $result =   array_key_exists($key,$array);
                    break;
            }
        }
        return (boolean)$result;
    }
}

if( !function_exists('resolve_url') ){
    /**
     * @ 解析url
     * @param $domain
     * @return array
     */
    function resolve_url ($url)
    {
        preg_match(
            "/^(?:([^:]+):\/\/)?(?:(?:([^\.]+)\.)?([^\.]+)\.([^\/]+)[\/]?)([^\?^#]+)?(?:[\?]([^#]+))?(?:[#](.*))?$/",
            $url,
            $matches
        );

        return [
            'origin'        =>  $url,
            'protocol'      =>  key_exists(1,$matches) ? $matches[1] : '',
            'prefix'        =>  key_exists(2,$matches) ? $matches[2] : '',
            'main'          =>  key_exists(3,$matches) ? $matches[3] : '',
            'postfix'       =>  key_exists(4,$matches) ? $matches[4] : '',
            'path'          =>  key_exists(5,$matches) ? $matches[5] : '',
            'params'        =>  key_exists(6,$matches) ? $matches[6] : '',
            'anchor'        =>  key_exists(7,$matches) ? $matches[7] : '',
        ];
    }
}

if( !function_exists('curl') ){

    function curl ($options=[],$post_data=[])
    {
        // 默认配置
        $default_aoptions               =   [
            CURLOPT_URL                     =>  false,             // 请求参数
            CURLOPT_RETURNTRANSFER          =>  true,           // return web page
            CURLOPT_HEADER                  =>  false,          // don't return headers
            CURLOPT_FOLLOWLOCATION          =>  true,           // follow redirects
            CURLOPT_ENCODING                =>  "",             // handle all encodings
            CURLOPT_USERAGENT               =>  "spider",       // who am i
            CURLOPT_AUTOREFERER             =>  true,           // set referer on redirect
            CURLOPT_CONNECTTIMEOUT          =>  120,            // timeout on connect
            CURLOPT_TIMEOUT                 =>  120,            // timeout on response
            CURLOPT_MAXREDIRS               =>  10,             // stop after 10 redirects
            CURLOPT_POST                    =>  0,              // i am sending post data
            CURLOPT_POSTFIELDS              =>  $post_data,     // this are my post vars
            CURLOPT_SSL_VERIFYHOST          =>  0,              // don't verify ssl
            CURLOPT_SSL_VERIFYPEER          =>  false,          //
            CURLOPT_VERBOSE                 =>  1,              //
            CURLOPT_USERPWD                 =>  false,
        ];
        // 执行参数
        $_options                       =   array_replace_recursive( $default_aoptions, $options );
        $_options[CURLOPT_URL] || E('请求路径缺失');
        // 初始化curl会话
        $ch                             =   curl_init();
        // 参数配置
        curl_setopt_array( $ch, $_options );
        // 执行
        $exec                           =   curl_exec($ch);
        // 结果处理
        $result['exec']                 =   $exec;
        if( $exec === false ){
            // 错误代号
            $result['errno']            =   curl_errno($ch);
            // 错误信息
            $result['error']            =   curl_error($ch);
        }
        // 句柄的信息
        $result['info']                 =   curl_getinfo($ch);
        return $result;
    }
}

if( !function_exists( 'Ajax' ) ){
    /**
     * Ajax方式返回数据到客户端
     * @access protected
     * @param mixed $data 要返回的数据
     * @param String $type AJAX返回数据格式
     * @param int $json_option 传递给json_encode的option参数
     * @return void
     */
    function Ajax ($data,$type='',$json_option=0)
    {
        if(empty($type)) $type  =   C('DEFAULT_AJAX_RETURN');
        switch (strtoupper($type)){
            case 'JSON' :
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                exit(json_encode($data,$json_option));
            case 'XML'  :
                // 返回xml格式数据
                header('Content-Type:text/xml; charset=utf-8');
                exit(xml_encode($data));
            case 'JSONP':
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                $handler  =   isset($_GET[C('VAR_JSONP_HANDLER')]) ? $_GET[C('VAR_JSONP_HANDLER')] : C('DEFAULT_JSONP_HANDLER');
                exit($handler.'('.json_encode($data,$json_option).');');
            case 'EVAL' :
                // 返回可执行的js脚本
                header('Content-Type:text/html; charset=utf-8');
                exit($data);
            default     :
                // 用于扩展其他返回格式数据
                \Think\Hook::listen('ajax_return',$data);
        }
    }
}

if( !function_exists( 'pattern' ) ){

    /**
     * @param $pattern
     * @param $subject
     * @param bool $all
     * @param array|null $matches
     * @param int $flags
     * @param int $offset
     * @return false|int
     */
    function pattern ($pattern, $subject, $all=false, array &$matches=null, $flags = 0, $offset = 0  )
    {
        return $all===false
            ?   preg_match( $pattern, $subject,$matches, $flags, $offset )
            :   preg_match_all( $pattern, $subject,$matches, $flags, $offset );
    }
}

if( !function_exists('maps') ) {
    /**
     * @param $lists
     * @param string $index_key
     * @return array
     */
    function maps ($lists, $index_key='id'){
        $maps           =   [];
        foreach ($lists as $value){
            if( $value[$index_key] )
                $maps[ $value[$index_key] ]     =   $value;
        }

        return $maps;
    }
}

if( !function_exists('script') ) {
    /**
     * @param $script
     * @return string
     */
    function script ($script) {
        return <<<SCRIPT
<script type="text/javascript">
    {$script}
</script>
SCRIPT;
    }
}

if( !function_exists('bool_value') ){
    function bool_value ($value) {
        switch ($value) {
            case 'true':
                return true;
            case 'false':
                return false;
            default:
                return (bool)$value;
        }
    }
}

if( !function_exists('http_code') ){
    function http_code ($code) {
        $code           =   (int)$code;
        $http_code      =   [
            100 => 'Continue',
            101 => 'Switching Protocols',

            // Success 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',

            // Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',  // 1.1
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            // 306 is deprecated but reserved
            307 => 'Temporary Redirect',

            // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            418 => 'Param Lose',
            419 => 'Authorized Failed',

            // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            509 => 'Bandwidth Limit Exceeded',
            510 => 'It is being maintained. Please try again later.',
        ];
        if( exists_key($code, $http_code) )
            return $http_code[ $code ];
        return false;
    }
}

if( ! function_exists('QueueEcho') ) {
    /**
     *  伴随滞后任务的输出
     * @param null $data
     */
    function QueueEcho( $data=null, $exec )
    {
        $contents = is_null($data) ? '' : json_encode($data);
        // 清空缓存
        ob_end_flush();
        // 再次开启
        ob_start();
        echo $contents;

        header("Content-Type:application/json;charset=utf-8");
        header("Connection:close");
        header('Content-Encoding:LaoZhou');
        header('Content-Length:'. ob_get_length());
        // 冲刷出缓存区
        ob_flush();
        // 输出到客户端
        flush();
        // 滞后执行
        $exec();
        exit;
    }
}

if( ! function_exists('yuan_img') )  {
    /**
     * 生成圆形图片
     * @param $imgpath  图片地址/支持微信、QQ头像等没有后缀的网络图
     * @param $saveName string 保存文件名，默认空。
     * @return resource 返回图片资源或保存文件
     */
    function yuan_img($imgpath,$saveName = '') {
        $src_img = imagecreatefromstring(file_get_contents($imgpath));
        $w = imagesx($src_img);$h = imagesy($src_img);
        $w = $h = min($w, $h);

        $img = imagecreatetruecolor($w, $h);
        //这一句一定要有
        imagesavealpha($img, true);
        //拾取一个完全透明的颜色,最后一个参数127为全透明
        $bg = imagecolorallocatealpha($img, 255, 255, 255, 127);
        imagefill($img, 0, 0, $bg);
        $r   = $w / 2; //圆半径
        for ($x = 0; $x < $w; $x++) {
            for ($y = 0; $y < $h; $y++) {
                $rgbColor = imagecolorat($src_img, $x, $y);
                if (((($x - $r) * ($x - $r) + ($y - $r) * ($y - $r)) < ($r * $r))) {
                    imagesetpixel($img, $x, $y, $rgbColor);
                }
            }
        }

        //输出图片到文件
        imagepng ($img,$saveName);
        //释放空间
        imagedestroy($src_img);
        imagedestroy($img);
    }

}

if( !function_exists('is_mobile_request') ) {

    /**
     * @return bool
     */
    function is_mobile_request(){
        $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';
        $mobile_browser = '0';
        if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
            $mobile_browser++;
        if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false))
            $mobile_browser++;
        if(isset($_SERVER['HTTP_X_WAP_PROFILE']))
            $mobile_browser++;
        if(isset($_SERVER['HTTP_PROFILE']))
            $mobile_browser++;
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
            'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
            'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
            'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda','xda-'
        );
        if(in_array($mobile_ua, $mobile_agents))
            $mobile_browser++;
        if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false)
            $mobile_browser++;
        // Pre-final check to reset everything if the user is on Windows
        if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false)
            $mobile_browser=0;
        // But WP7 is also Windows, with a slightly different characteristic
        if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false)
            $mobile_browser++;
        if($mobile_browser>0)
            return true;
        else
            return false;
    }
}