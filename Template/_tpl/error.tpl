
<style>
    body {
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    a {
        text-decoration: none;
    }

    .content {
        width: 100%;
        text-align: center;
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }

    .img_ {
        width: 500px;
        height: 239px;
        margin: 0 auto;
        margin-bottom: 2%;
        position: relative;
        left: 107px;
    }

    .img_>img:nth-of-type(1) {
        position: absolute;
        left: 0;
    }

    #astronaut {
        position: absolute;
        right: 85px;
        top: 0;
    }

    .content>p {
        font-size: 32px;
        color: #772c2c;
    }

    .but_>span {
        color: #4FB674;
    }

    .but_>a {
        display: inline-block;
        width: 180px;
        height: 44px;
        text-align: center;
        line-height: 44px;
        font-size: 16px;
        border-radius: 3px;
        color: #58AA9E;
        border: 1px solid #6DA49C;
    }

    .but_>a>img {
        vertical-align: middle;
        margin-right: 8px;
    }

    .but_>a:nth-of-type(2) {
        color: white;
        background: #58AA9E;
        margin-left: 30px;
    }

    @media only screen and (max-width: 700px) {
        .content>img {
            width: 40px;
            margin-bottom: 20px;
        }
        .content>p {
            font-size: 20px;
            color: #666666;
            margin-bottom: 40px;
        }
        .but_>a {
            display: inline-block;
            width: 120px;
            height: 35px;
            text-align: center;
            line-height: 35px;
            font-size: 14px;
            border-radius: 3px;
            color: #58AA9E;
            border: 1px solid #6DA49C;
        }
        .but_>a>img {
            vertical-align: middle;
            margin-right: 5px;
            width: 20px;
        }
        .img_ {
            width: 300px;
            height: 170px;
            margin: 0 auto;
            margin-bottom: 2%;
            position: relative;
            left: 66px;
        }
        .img_>img:nth-of-type(1) {
            position: absolute;
            left: 0;
            width: 150px;
        }
        #astronaut {
            position: absolute;
            top: 15px;
            width: 160px;
        }
    }

    @media only screen and (max-width: 300px) {
        .content>img {
            width: 20px;
            margin-bottom: 0px;
        }
        .content>p {
            font-size: 12px;
            color: #666666;
            margin-bottom: 20px;
        }
        .but_>a {
            display: inline-block;
            width: 90px;
            height: 30px;
            text-align: center;
            line-height: 30px;
            font-size: 12px;
            border-radius: 3px;
            color: #58AA9E;
            border: 1px solid #6DA49C;
        }
        .but_>a>img {
            vertical-align: middle;
            margin-right: 3px;
            width: 10px;
        }
        .but_>a:nth-of-type(2) {
            color: white;
            background: #58AA9E;
            margin-left: 10px;
        }
        .img_ {
            width: 200px;
            height: 110px;
            margin: 0 auto;
            margin-bottom: 2%;
            position: relative;
            left: 43px;
        }
        .img_>img:nth-of-type(1) {
            position: absolute;
            left: 0;
            width: 100px;
        }
        #astronaut {
            position: absolute;
            top: 20px;
            width: 90px;
        }
    }
</style>

<body>
<div style="display: none;">
    <input id="error_val" type="hidden" value="{$error|default='信号懵逼了，请稍后再试…'}"/>
    <input id="sec_val" type="hidden" value="{$waitSecond|default=1}">
    <input id="url_val" type="hidden" value="{$jumpUrl}"/>
</div>
<div class="content">
    <div class="img_">
        <img src="../../../Public/_tpl/imgs/Group2.png" />
        <img id="astronaut" class="pelm top" src="../../../Public/_tpl/imgs/Group8.png" />
    </div>
    <p id="error" class="message"></p>
    <div class="but_">
        <a href="javascript:history.back(-1);" class="renovate"><img src="../../../Public/_tpl/imgs/refrash.png" />返回上一级</a>
        <a href="/"><img src="../../../Public/_tpl/imgs/home.png" />返回首页</a>
    </div>
</div>

</body>
<script src="../../../Public/Common/js/jquery-3.2.0.min.js"></script>
<script>
    document.querySelector("html").style.height = 100 + "%";
    var error_val               =   document.getElementById('error_val').value,
        sec_val                 =   document.getElementById('sec_val').value,
        url_val                 =   document.getElementById('url_val').value,
        wait = document.getElementById('sec'),
        jump = document.getElementById('jump'),
        error = document.getElementById('error');

    error.innerHTML     =   error_val;

    ! function(a, b, c, d) {
        "use strict";

        function e(b, c) {
            this._name = f, this._defaults = g, this.element = b, this.options = a.extend({}, g, c), this.init()
        }
        var f = "parallaxmouse",
            g = {
                range: 100,
                elms: [],
                invert: !1
            };
        a.extend(e.prototype, {
            init: function() {
                this.element = a(this.element), this.setInitialPositions(this.options.elms), this.parallaxElms(this.options.elms)
            },
            setInitialPositions: function() {
                a(this.options.elms).each(function(a, b) {
                    var c = b.el.position();
                    b.el.hasClass("left") ? (b.x = c.left, b.left = !0) : (b.x = parseInt(b.el.css("right").replace("px")), b.left = !1), b.el.hasClass("top") ? (b.y = c.top, b.top = !0) : (b.y = parseInt(b.el.css("bottom").replace("px")), b.top = !1)
                })
            },
            parallaxElms: function() {
                var b = this;
                this.element.on("mousemove", function(c) {
                    var d = b.element.outerWidth(),
                        e = b.element.outerHeight(),
                        f = c.clientX,
                        g = c.clientY,
                        h = f / d,
                        i = g / e;
                    a(b.options.elms).each(function(a, c) {
                        var d = b.options.invert,
                            e = b.options.range * h,
                            f = b.options.range * i,
                            g = d ? c.x + e : c.x - e,
                            j = d ? c.y + f : c.y - f,
                            k = g * c.rate,
                            l = j * c.rate;
                        c.left ? c.el.css("left", c.x + -1 * k) : c.el.css("right", c.x + k), c.top ? c.el.css("top", c.y + -1 * l) : c.el.css("bottom", c.y + l)
                    })
                })
            }
        }), a.fn[f] = function(b) {
            return this.each(function() {
                a.data(this, "plugin_" + f) || a.data(this, "plugin_" + f, new e(this, b))
            })
        }
    }(jQuery, window, document);
    $(window).parallaxmouse({
        invert: true,
        range: 100,
        elms: [{
            el: $('#astronaut'),
            rate: 0.48
        }, ]
    });
</script>
