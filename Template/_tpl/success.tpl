
<style>
    body {
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    a {
        text-decoration: none;
    }

    .content {
        width: 100%;
        text-align: center;
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }


    .content>p {
        font-size: 32px;
        color: #666666;
    }
    .sec{
        color: #4fb674;
    }

    .content>div>a {
        display: inline-block;
        width: 180px;
        height: 44px;
        text-align: center;
        line-height: 44px;
        font-size: 16px;
        border-radius: 3px;
        color: #58AA9E;
        border: 1px solid #6DA49C;
        margin-right: 30px;
    }

    .content>div>a>img {
        vertical-align: middle;
        margin-right: 8px;
        position: relative;
        top: -2px;
    }

    .home {
        color: white !important;
        background: #58AA9E;
        margin-right: 0 !important;
    }

    @media only screen and (max-width: 500px) {
        .content>img {
            width: 40px;
            margin-bottom: 20px;
        }
        .content>p {
            font-size: 20px;
            color: #666666;
            margin-bottom: 40px;
        }
        .content>div>a {
            display: inline-block;
            width: 120px;
            height: 35px;
            text-align: center;
            line-height: 35px;
            font-size: 14px;
            border-radius: 3px;
            color: #58AA9E;
            border: 1px solid #6DA49C;
        }
        .content>div>a>img {
            vertical-align: middle;
            margin-right: 5px;
            width: 20px;
        }
    }

    @media only screen and (max-width: 300px) {
        .content>img {
            width: 20px;
            margin-bottom: 0px;
        }
        .content>p {
            font-size: 12px;
            color: #666666;
            margin-bottom: 20px;
        }
        .content>div>a {
            display: inline-block;
            width: 90px;
            height: 30px;
            text-align: center;
            line-height: 30px;
            font-size: 12px;
            border-radius: 3px;
            color: #58AA9E;
            border: 1px solid #6DA49C;
        }
        .content>div>a>img {
            vertical-align: middle;
            margin-right: 3px;
            width: 10px;
        }
        .content>div>a:nth-of-type(2) {
            color: white;
            background: #58AA9E;
            margin-left: 10px;
        }
    }
</style>

<body>
<div style="display: none;">
    <input id="message_val" type="hidden" value="{$message|default='操作成功'}"/>
    <input id="sec_val" type="hidden" value="{$waitSecond|default=1}"/>
    <input id="url_val" type="hidden" value="{$jumpUrl|default='javascript:;'}"/>
</div>
<div class="content">
    <img src="../../../Public/_tpl/imgs/cucc.png" />
    <p><span id="message" class="message"></span>(<span id="sec" class="sec"></span> s) </p>
    <div>
        <a id="jump" href="javascript:;" class="renovate"><img src="../../../Public/_tpl/imgs/jump.png" />立即跳转</a>
        <a href="/" class="home"><img src="../../../Public/_tpl/imgs/home.png" />返回首页</a>
    </div>
</div>
</body>
<script type="text/javascript">
    document.querySelector("html").style.height = 100 +"%";
    (function(){
        //  获取值
        var message_val             =   document.getElementById('message_val').value,
            sec_val                 =   document.getElementById('sec_val').value,
            url_val                 =   document.getElementById('url_val').value,
            wait = document.getElementById('sec'),
            jump = document.getElementById('jump'),
            message = document.getElementById('message');
        //  赋值
        //      页面跳转
        if( url_val != 'javascript:;' ) {
            jump.href=url_val;
            function callback () {
                location.href = url_val;
            }
        }else{
            //  执行javascript
            jump.style.display  =   'none';
            function callback () {
                //      打印记录
                console.log('Do callback.');
                //      执行自定义
                {$script}
            }
        }
        message.innerHTML       =   message_val;
        wait.innerHTML          =   sec_val;
        var interval = setInterval(function(){
            var time = --wait.innerHTML;
            if(time <= 0) {
                callback()
                clearInterval(interval);
            };
        }, 1000);
    })();

</script>
