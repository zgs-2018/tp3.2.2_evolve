<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<style>
    body {
        width: 100%;
        height: 100%;
    }

    a {
        text-decoration: none;
    }

    .content {
        width: 100%;
        text-align: center;
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }

    .img_{
        width: 500px;
        height: 239px;
        margin:0 auto;
        margin-bottom: 2%;
        position: relative;
    }

    .content>p {
        font-size: 32px;
        color: #666666;
    }

    .but_>span {
        color: #4FB674;
    }

    .but_>a {
        display: inline-block;
        width: 180px;
        height: 44px;
        text-align: center;
        line-height: 44px;
        font-size: 16px;
        border-radius: 3px;
        color: #58AA9E;
        border: 1px solid #6DA49C;
    }

    .but_>a>img {
        vertical-align: middle;
        margin-right: 8px;
    }

    .but_>a:nth-of-type(2) {
        color: white;
        background: #58AA9E;
        margin-left: 30px;
    }

    @media only screen and (max-width: 700px) {
        .content>img {
            width: 40px;
            margin-bottom: 20px;
        }
        .content>p {
            font-size: 20px;
            color: #666666;
            margin-bottom: 40px;
        }
        .but_>a {
            display: inline-block;
            width: 120px;
            height: 35px;
            text-align: center;
            line-height: 35px;
            font-size: 14px;
            border-radius: 3px;
            color: #58AA9E;
            border: 1px solid #6DA49C;
        }
        .but_>a>img {
            vertical-align: middle;
            margin-right: 5px;
            width: 20px;
        }

        .img_{
            width: 500px;
            height: 170px;
            margin:0 auto;
            margin-bottom: 2%;
            position: relative;
        }
        .img_>img:nth-of-type(1){
            position: absolute;
            left: 0;
            width: 500px;

        }
    }

    @media only screen and (max-width: 500px) {
        .content>img {
            width: 20px;
            margin-bottom: 0px;
        }
        .content>p {
            font-size: 12px;
            color: #666666;
            margin-bottom: 20px;
        }
        .but_>a {
            display: inline-block;
            width: 90px;
            height: 30px;
            text-align: center;
            line-height: 30px;
            font-size: 12px;
            border-radius: 3px;
            color: #58AA9E;
            border: 1px solid #6DA49C;
        }
        .but_>a>img {
            vertical-align: middle;
            margin-right: 3px;
            width: 10px;
        }
        .but_>a:nth-of-type(2) {
            color: white;
            background: #58AA9E;
            margin-left: 10px;
        }

        .img_{
            width: 200px;
            height: 110px;
            margin:0 auto;
            margin-bottom: 2%;
            position: relative;
        }
        .img_>img:nth-of-type(1){
            position: absolute;
            left: 0;
            width: 200px;

        }
    }
</style>

<body>
<div class="content">
    <div class="img_">
        <img src="../../../Public/_tpl/imgs/Group15.png"/>
    </div>
        <?php if(APP_DEBUG):?>
            <p style="font-size: 12px;color: #C40000"><?php echo strip_tags($e['message']);?></p>
        <?php else:?>
            <p>哎呀，页面不存在，程序猿正在帮您找回中…</p>
        <?php endif;?>
    <div class="but_">
        <a href="javascript:history.back(-1);"><img src="../../../Public/_tpl/imgs/refrash.png" />返回上一级</a>
        <a href="/"><img src="../../../Public/_tpl/imgs/home.png" />首页</a>
    </div>
</div>
</body>
<script>
    document.querySelector("html").style.height = 100 +"%"
</script>

</html>