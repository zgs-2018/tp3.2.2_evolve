## 授权模块
*   票有效期为 3600
#### 客户端
*   ID          appid
*   SECRET      appsecret
*   SID         会话id
*   URL         回调url


#### 服务端
*   ticket      票
*   SID         客户端会话id
*   openID      用户openID （冗余）
*   服务端session有效期一定要大于票的有效期

#### 授权拦截中间件
*   登录状态
*   未登录 --> 远程是否登录
*   远程登录（客户端本地有票）&&（票在远程合法）--> 执行本地登录
*   远程未登录 （本地无票）|| （远程票不合法）--> 跳转服务端授权

## 接口返回值
*   result boolen 结果
*   status(code) int 状态码
*   error string 错误信息
*   message string 提示信息
*   data array 返回数据
``` php
    // 正确
    $result         =   [
        'result'=>true,'status'=>200,'message'=>'','data'=>[],
    ];
    // 错误
    $result         =   [
        'result'=>false,'status'=>403,'error'=>'',
    ];
   
```