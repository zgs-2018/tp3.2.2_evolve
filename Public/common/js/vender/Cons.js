/**
 * 控制台打印
 *
 * @version 1.0
 */

class Cons
{
    constructor () {
        this.operate            =   console;
        this.outStyles          =   [];
    }

    /**
     * 颜色
     * @param color
     * @returns {Cons}
     */
    color (color) {
        this.style      =   "color: " + color;
        return this;
    }

    /**
     * 大小
     * @param size
     * @returns {Cons}
     */
    size (size) {
        if( !size ) size = 1;
        this.style      =   "font-size: " + size + "em";
        return this;
    }

    oblique () {
        this.style      =   "font-style: oblique";
        return this;
    }

    /**
     * 添加阴影
     * @returns {Cons}
     */
    shadow () {
        this.style      =   `
        text-shadow: 0 1px 0 #ccc,
        0 2px 0 #c9c9c9,
        0 3px 0 #bbb,
        0 4px 0 #b9b9b9,
        0 5px 0 #aaa,
        0 6px 1px rgba(0,0,0,.1),
        0 0 5px rgba(0,0,0,.1),
        0 1px 3px rgba(0,0,0,.3),
        0 3px 5px rgba(0,0,0,.2),
        0 5px 10px rgba(0,0,0,.25),
        0 10px 10px rgba(0,0,0,.2),
        0 20px 20px rgba(0,0,0,.15)
        `;
        return this;
    }

    /**
     * 输出
     * @param content
     * @param custom
     */
    say (content, custom=[]) {
        let outStyles       =   this.style.concat( custom );
        outStyles           =   outStyles.join( ';' );
        if( typeof content == 'object' ) {
            content         =   content.join('\n');
        }
        this.operate.log( "%c" + content, outStyles  );
        this.outStyles      =   [];
    }

    /**
     * 清除
     */
    clear () {
        this.operate.clear();
        return this;
    }

    set style (style) {
        this.outStyles.push( style );
    }

    get style () {
        return this.outStyles;
    }
}

