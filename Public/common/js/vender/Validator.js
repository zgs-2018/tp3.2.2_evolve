

/**
 * @ 构造方法
 * @param fields
 * @param denger
 * @returns {Validator}
 * @constructor
 */
var Validator = function (fields,denger) {
    var that            =   this;
    // 验证配置
    that.Config =   {
        Verify: {
            required:function (node) {
                return that.nodeValue(node).toString().length > 0
                    ? true
                    : that.setErrorMessage(node,'不能为空');
            }
            ,mobile:function (node) {
                var value       =   that.nodeValue(node);
                // 为空不验证
                if( value == '' )   return true;
                return that.Config._Regular.phone.test(value)
                    ? true
                    : that.setErrorMessage(node,'不合法');
            }
            ,cmobile:function (node) {
                var value       =   that.nodeValue(node);
                // 为空不验证
                if( value == '' )   return true;
                return that.Config._Regular.cmobile.test(value)
                    ? true
                    : that.setErrorMessage(node,'不合法');
            }
            ,max:function (node,param) {
                var value       =   that.nodeValue(node);
                // 为空不验证
                if( value == '' )   return true;
                return value.length <= param
                    ? true
                    : that.setErrorMessage(node,'超出最大限制'+param);
            }
            ,min:function (node,param) {
                var value       =   that.nodeValue(node);
                // 为空不验证
                if( value == '' )   return true;

                return value.length >= param
                    ? true
                    : that.setErrorMessage(node,'小于最小限制'+param);
            }
            ,in:function (node,param) {
                var allow       =   param.split(','),
                    value       =   that.nodeValue(node),
                    result      =   false;
                // 为空不验证
                if( value == '' )   return true;
                allow.forEach(function(item){
                    if( item == value ){
                        result =    true;
                        return;
                    }
                });
                return result
                    ? true
                    : that.setErrorMessage(node,value+"不在"+param+'范围之内');
            }
            ,multi:function (node,param) {
                var allow       =   param.split(','),
                    value       =   that.nodeValue(node),   //array
                    result      =   true,
                    trigger     =   ''    ;
                // 为空不验证
                if( value == '' || value == 'undefined' ){
                    return true;
                }
                value.forEach( function (v) {
                    if( allow.indexOf(v) == -1 ){
                        trigger = v;
                        result = false; return ;
                    }
                } );

                return result
                    ? true
                    : that.setErrorMessage(node,trigger+"不在"+param+'范围之内');
            }
            ,number:function (node,param) {
                var value       =   that.nodeValue(node);
                // 为空不验证
                if( value == '' )   return true;
                return /^[0-9]+$/.test(value)
                    ?   true
                    :   that.setErrorMessage(node,'不是数字');
            }
            ,integer:function (node,param) {
                var value       =   that.nodeValue(node);
                // 为空不验证
                if( value == '' )   return true;
                return /^[0-9]+$/.test(value)
                    ?   true
                    :   that.setErrorMessage(node,'不是数字');
            }
            ,email:function (node) {
                var value       =   that.nodeValue(node);
                // 为空不验证
                if( value == '' )   return true;

                return that.Config._Regular['email'].test(value)
                    ?   true
                    :   that.setErrorMessage(node,'格式不对');
            }
            ,same:function (node,param) {
                var value       =   that.nodeValue(node),
                    sameValue   =   that.nodeValue(param);
                if( !param )    return false;

                return ( value === sameValue ) ? true : that.setErrorMessage(node, '两次输入不一致.');
            }
            ,length:function (node,param) {
                var value       =   that.nodeValue(node);
                // 为空不验证
                if( value == '' )   return true;
                return ( value.length() == param ) ? true : that.setErrorMessage(node, '长度不合法.');

            }
            ,between: function (node,param) {
                var value       =   that.nodeValue(node),
                    params      =   param.split(',');
                //  为空不验证
                if( value == '' )   return true;
                //  是否为数字类型
                if( ! this.number(node,param) )
                    return false;
                return ( (params[0] <= value ) && (value <= params[1]) ) ? true : that.setErrorMessage(node, '不在范围之间');
            },
            bail: function (node,param) {
                return true;
            },
        }
        // 正则
        ,_Regular:{
            phone: /^1[34578][0-9]{9}$/,
            cmobile: /^[0-9]{6,14}$/,
            url: /(^#)|(^http(s*):\/\/[^\s]+\.[^\s]+)/,
            number:/^[1-9][0-9]*$/,
            email:/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/
        }
        // 字段中文映射
        ,_Map: {
            name        :   '姓名'
            ,mobile     :   '手机号码'
            ,nl         :   '日语等级'
            ,year       :   '出国年份'
            ,come       :   '来校咨询'
            ,email      :   '邮箱'
            ,password   :   '密码'
            ,code       :   '验证码'
        }
    };
    that.fields             =   fields;                     // 验证字段
    that.denger             =   denger ? true : false;      // 是否抛出异常
    that.errorMessage       =   '';                         // 内部错误信息
    that.errorNode          =   '';                         // 内部错误节点
    that.showError          =   '';                         // 外部错误信息
    that.rules              =   [];                         // 储存验证规则
    that.passed             =   {};                         // 验证后字段数据信息

    return that;
};

/**
 * @ 验证方法
 * @param config
 * @returns {*}
 * @constructor
 */
Validator.prototype.Verify = function (config) {
    var that                    =   this,
        resolvedRules           =   that.Resolve( config );     // 解析验证规则 return Array
    try{
        // 遍历字段 验证字段下定义规则
        Object.keys(resolvedRules).forEach(function (key) {
            var currentRules            =   resolvedRules[key];
            Object.keys(currentRules).forEach( function (k,i) {
                // 当前字段下 子验证规则
                var subRule         =   currentRules[k],
                    subType         =   subRule['type'],
                    subParam        =   subRule.length == 1 ? '' : subRule['param'];

                that.Config.Verify[subType](key,subParam) || that.Exception(that.errorMessage,key);
            } );
            that.passed[key]        =   that.nodeValue(key);
        });

        return that.passed;
    }catch (E){
        // console.log(E);
        that.showError = E.message;
        return false;
    }
};

/**
 * @ 参数处理
 * @param config
 * @constructor
 */
Validator.prototype.Resolve = function (config) {
    var that            =   this;
    Object.keys(config).forEach( function (key,index) {
        // 分割当前字段的验证规则
        var basis           =   config[key].split("|"),
            current         =   [];
        // 遍历处理子规则
        basis.forEach( function (k, v) {
            var spilt       =   k.split(':'),
                item        =   [];
            // current.isPass  =   false;           // 忽略字段验证
            item['type']            =   spilt[0];
            item['param']           =   spilt.length == 1 ? '' : spilt[1] ;
            current.push( item );
        } );
        that.rules[key]     =   current;
    } );

    return that.rules;
};

/**
 * @ 获取节点值
 * @param node
 * @returns {*}
 */
Validator.prototype.nodeValue = function (node) {
    var that            =   this;
    return that.fields.hasOwnProperty(node)
        ?   that.fields[node]
        :   that.nodeMultiValue(node);
};

/**
 * @ 获取多功能节点值
 * @param node
 */
Validator.prototype.nodeMultiValue = function (node) {
    var that            =   this,
        fields          =   that.fields,
        value           =   [],
        reg             =   eval("/^"+node+"\\[[0-9]+\\]$/");

    Object.keys(fields).forEach( function (alias) {
        if( reg.test(alias) )  value.push( fields[alias] );
    } );

    return value;
};

/**
 * @ 异常抛出
 * @param message
 * @param node  节点
 * @constructor
 */
Validator.prototype.Exception = function (message,node) {
    throw new Error(message);
};

/**
 * @ 设置内部错错误信息
 * @param message
 * @returns {boolean}
 */
Validator.prototype.setErrorMessage = function (node,message){
    var that        =   this,
        alias       =   that.Config._Map.hasOwnProperty(node)
            ? that.Config._Map[node]
            : node;
    that.errorNode      =   node;
    that.errorMessage   =   alias+message;
    return false;
}
