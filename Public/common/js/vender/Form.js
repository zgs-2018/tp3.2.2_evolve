class Form {
    /**
     * 构造
     * @param fields    初始化定义字段
     * @param container 表单容器
     * @param tplObj    模板容器
     */
    constructor (fields,container,tplObj='#form-tpl') {
        this._fields        =   fields;
        this.container      =   container;
        this.tplObj         =   tplObj;
        this.initApi        =   Config.API_DOMAIN+'/form/init';
        this.remoteFeilds   =   '';
        this.init();
    }

    /**
     * 初始化
     */
    init () {
        var _this               =   this,
            init_data           =   {
                _fields: _this._fields
            };
        //  获取表单信息
        Mc.jsonpRequest( _this.initApi, init_data, function (res) {
            if( res.result ) {
                //  存储token
                _this.setToken(res.data._token);
                //  储存远程fields信息
                _this.remoteFeilds      =   res.data.fields;
                _this.remoteAddress     =   res.data.address;
                //  创建表单
                _this.buildForm( res.data.fields, _this.container );
            } else {
                Mc.log( res );
            }
        } , false );
        return true;
    }

    /**
     * @param fields    返回字段列表
     * @param container 表单容器
     */
    buildForm(fields,container) {
        var _this               =   this,
            _html               =   '';
        $.each( fields ,function (index,value) {
            _html       +=  _this.buildUnitByType( value );
        } );

        $( container ).html(_html);
    }

    /**
     * 构造单元组件
     * @param value
     */
    buildUnitByType (value) {
        var _this               =   this,
            //  div节点
            tplObj              =   $("#form-tpl").find( $('*[tpl*="'+value.type+'"]') ),
            //  表单元素节点
            unitObj             =   tplObj.children() ;
        if( ! tplObj.length )   return ;
        switch (value.type) {
            case 1:
                //  文本
                unitObj.attr('id', value.key);
                unitObj.attr('name', value.key );
                unitObj.attr('placeholder', value.desc );
                return tplObj.html();
            case 2:
                //  下拉
                unitObj.attr('id', value.key);
                unitObj.attr('name', value.key );
                var option      =   unitObj.children();
                option.html(value.desc);
                $.each(value.values, function (ind,val) {
                    var newOption       =   option.clone();
                    newOption.val(val.id);
                    newOption.html(val.name);
                    unitObj.append(newOption);
                });
                return tplObj.html();
        }
    }

    /**
     * 存储token
     * @param token
     */
    setToken (token) {
        this.token      =   token;
    }

    /**
     * 获取token
     * @returns {*}
     */
    getToken () {
        return this.token;
    }

    submit (btn) {
        var _this           =   this,
            form_data       =   _this.fetchFormData (),
            regular         =   _this.fetchRegular(),
            validator       =   new Validator(form_data),
            _token          =   _this.token,
            data            =   {};
        //  前置执行
        _this._before_submit ();
        //  验证
        if( ! validator.Verify(regular) ) {
            _this.showError(validator.errorNode, validator.errorMessage)
            return false;
        }
        //  数据填充
        form_data           =   _this.fillFormData(form_data);
        data['_data']       =   form_data;
        data['_token']      =   _token;
        //  提交
        Mc.jsonpRequest( _this.remoteAddress.submit, data, function (res) {
            if( res.result ) {
                //  摧毁表单
                _this.destroyContainer();
                $(btn).html('请等待老师联系.');
                $(btn).removeAttr('onclick');
            } else {
                console.log( '提交失败', res );
            }
        } );
    }

    /**
     * 提交前置
     * @private
     */
    _before_submit () {
        //  移除报错
        $('.show_form_error').removeClass('show_form_error');
    }

    /**
     * 获取表单值
     * @returns {string}
     */
    fetchFormData () {
        var _this           =   this,
            init_fields     =   _this._fields,
            form_data       =   {};
        $.each(init_fields, function (ind,val) {
            let vv      =   $("*[id*='"+val+"']").val();
            form_data[ val ]    =   vv;
        });
        return form_data;
    }

    /**
     * 获取验证规则
     * @returns {string}
     */
    fetchRegular () {
        var _this           =   this,
            init_fields     =   _this._fields,
            regular         =   {};
        $.each( init_fields, function (ind,val) {
            if( _this.remoteFeilds[val] ) {
                let vv      =    _this.remoteFeilds[val].valid;
                regular[ val ]      =  vv;
            }
        } );
        return regular;
    }

    /**
     * 数据填充
     * @param data
     * @returns {*}
     */
    fillFormData (data) {
        data[ 'OE.010' ]        =   location.href;
        return data;
    }

    /**
     *  错误提示
     * @param node
     * @param message
     */
    showError (node, message) {
        if( node ) {
            $("*[id*='"+node+"']").addClass('show_form_error');
        } else {
            $(".show_error").text(message);
        }
    }

    destroyContainer () {
        var _this               =   this;
        $(_this.container).html('');
    }
}

