
class Macro
{
    constructor () {
        this.RequestParams      =   this.parseUrl( location.href ).params;
    }

    /**
     * 解析domain
     * @param url
     * @returns {*}
     */
    parseUrl (url) {
        var url         =   url ? url : location.href,
            pattern     =   /^(http|https):\/\/(?:([^\.]*)\.)?([^\.]*)\.([^\/]*)(?:\/([^\?#]*)?(?:\?([^#]*))?(?:#(.*))?)?$/i,
            exec        =   pattern.exec(url),
            params      =   {};
        //  解析失败
        if( ! exec )  return false;
        //  参数解析
        if( exec[6] ) {
            var paramsArr       =   exec[6].split('&');
            paramsArr.forEach(function (value,index) {
                let item        =   value.split('=');
                if( item.length = 2 )
                    params[ item[0] ]   =   item[1];
            });
        }

        return {
            origin:     exec[0],
            protocol:   exec[1],
            prefix:     exec[2] ? exec[2] : '',
            main:       exec[3],
            suffix:     exec[4],
            uri:        exec[5] ? '/'+exec[5] : '/',
            params:     params,
            anchor:     exec[7] ? exec[7] : '',
        };
    }

    /**
     * 域内请求
     * @param url
     * @param data
     * @param success
     * @param method
     * @param async
     * @param error
     */
    request (url, data, success, method='POST', async=true,error={} ) {
        var _Macro          =   this;
        $.ajax({
            url: url,
            type: method,
            data: data,
            dataType: "json",
            async:async,
            success: function (res) {
                success(res);
            },
            error: function (e) {
                _Macro.log('ERR', e);
                return ;
            }
        });
    }

    /**
     * 跨域请求
     * @param url
     * @param data
     * @param success
     * @param async
     * @param error
     */
    jsonpRequest (url, data, success, async=true, error={}) {
        var _Macro          =   this;
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            dataType: "jsonp",
            async:async,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function (res) {
                success(res);
            },
            error: function (e) {
                _Macro.log('ERR', e);
                return ;
            }
        });
    }

    /**
     * 数据中心请求
     * @param url
     * @param data
     * @param success
     * @param async
     * @param error
     */
    depotsRequest (url, data, success, async=true, error={}) {
        var _this           =   this,
            access_token    =   $('meta[name="ACCESS_TOKEN"]').attr('content');
        data['access_token']=   access_token;
        _this.jsonpRequest(url, data, success, async, error);
    }

    /**
     * 分割数组|对象
     * @param data
     * @param size
     * @returns {Array}
     */
    array_chunk (data, size) {
        var result      =   [],
            data        =   data.length ? data : this.object2array(data),
            _len        =   data.length;
        for(var i=0;i<_len;i+=size){
            result.push(data.slice(i,i+size));
        }
        return result;
    }

    object2array ( object ) {
        var arr = []
        for (var i in object) {
            arr.push(object[i]);
        }
        return arr;
    }

    /**
     * 控制台打印
     * @param params
     */
    log (...params) {
        console.log( params );
    }
}