<?php
namespace Think;

use Think\Storage;

class Queue
{
    static private $_task = array();

    static private $_before = array();
    static private $_after = array();
    static private $_instence;
    static $errorIndex = 'error';

    public function __construct()
    {
        return (self::$_instence instanceof self) ? self::$_instence : (self::$_instence = new self());
    }


    /**
     *  置顶插入任务
     * @param null $executor
     * @param null $params
     * @return int
     */
    static public function before( $executor=null, $params=null )
    {
        return array_unshift( self::$_task,array( 'executor'=>$executor, 'params'=>$params, 'time'=>date('Y-m-d H:i:s') ) );
    }

    /**
     *  置后插入任务
     * @param null $executor
     * @param null $params
     * @return int
     */
    static public function after( $executor=null, $params=null )
    {
        return array_push( self::$_task,array('executor'=>$executor, 'params'=>$params, 'time'=>date('Y-m-d H:i:s')) );
    }

    /**
     *  任务列表
     * @return array
     */
    static public function task()
    {
        return self::$_task;
    }

    /**
     *  任务处理
     */
    static public function handle()
    {
        if( !self::$_task ) return ;

        foreach( self::$_task as $key => $task )
        {
            // 处理当前子任务
            self::handleRow( $key,$task );
        }
        self::doRecord(self::$_task);
    }

    /**
     * 单个任务处理
     * @param $key
     * @param $task
     */
    static private function handleRow($key,$task)
    {
        // 任务是否存在
        if( $task === null ) return false;

        // 任务处理
        $task = is_array($task['executor'])
            ? self::handleMethodTask( $key,$task )
            : self::handlefunctionTask( $key,$task );

        // 任务是否合法
        if( $task === false ) return false;

        // 任务执行
        return self::execute( $key,$task );
    }

    /**
     * 类方法任务处理
     * @param $key
     * @param $task
     * @return \ReflectionMethod
     */
    static private function handleMethodTask($key,$task)
    {
        // 赋值
        list($class,$method) = $task['executor'];
        // 构造反射方法对象
        try{
            $infoObj = new \ReflectionMethod($class,$method);
        }catch (\ReflectionException $e){
            // 记录错误信息
            self::$_task[$key][self::$errorIndex] = "{$class}::{$method} is not callable";
            return false;
        }
        // 参数绑定对象
        $infoObj->params = $task['params'];
        return $infoObj;
    }

    /**
     * 函数任务处理
     * @param $executor
     * @return bool
     */
    static private function handlefunctionTask($key,$task)
    {
        if( is_callable($task['executor']) )
            return $task;
        // 记录错误信息
        self::$_task[$key][self::$errorIndex] = $task['executor'].' is not callable';
        return false;
    }

    /**
     * 任务执行
     * @param $key
     * @param $task
     */
    static private function execute($key,$task)
    {
        // 执行时间
        self::$_task[$key]['execTime'] = date('Y-m-d H:i:s');
        // 执行、执行结果
        self::$_task[$key]['result'] = ( $task instanceof \ReflectionMethod )
            ?   $task->invokeArgs(new $task->class,$task->params)
            :   call_user_func_array($task['executor'], $task['params']);
        return ;
    }

    /**
     *  任务记录
     * @param $record
     * @return mixed
     */
    static private function doRecord($record)
    {
        $filename = RUNTIME_PATH.'Queue/'.date('Y-m-d');

        //DP(serialize($record).'\n');
        return \Think\Storage::append($filename,serialize($record)."\n");
    }

    /**
     * 返回任务记录
     * @param string $date
     * @return array
     */
    static public function record($date='now')
    {
        $date = date( 'Y-m-d',strtotime($date) );
        $filename = RUNTIME_PATH.'Queue/'.$date;

        return array_map('unserialize',file($filename));
    }

    static private function getTaskKey()
    {
        return str_replace('.','_',uniqid( microtime(true), true ));
    }

    private function _clone() {}
}